<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction';

    public function article()
    {
        return $this->belongsTo('App\Article', 'article_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getArticleName()
    {
        return Article::find($this->article_id)->title;
    }

    public function getUserName()
    {
        return User::find($this->user_id)->name;
    }
}
