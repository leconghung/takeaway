<?php

namespace App\Http\Middleware;

use Closure;

class Online
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ip = $request->getClientIp();
        $check = \App\Online::where('ip', $ip)->where('expired_time', '>=', date('Y-m-d H:i:s'))->first();
        if (count($check) == 0) {
            $new = new \App\Online();
            $new->ip = $ip;
            $new->expired_time = date('Y-m-d H:i:s', strtotime('+1 hour'));
            $new->save();
        }
        return $next($request);
    }
}
