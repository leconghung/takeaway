<?php

namespace App\Http\Controllers;

use App\Online;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('account.register');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login()
    {
        return view('account.login');
    }

    /**
     * @return $this
     */
    public function logout()
    {
        return redirect()->back()
            ->withCookies([Cookie::forget('login_flag'), Cookie::forget('user_name'), Cookie::forget('user_id'), Cookie::forget('fresh_id')]);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function loginPost(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'username' => 'min:4',
                'password' => 'min:6'
            ),
            array(
                'username.min' => 'Tên đăng nhập phải có tối thiếu 3 ký tự!',
                'password.min' => 'Mật khẩu phải có tối thiếu 6 ký tự!'
            ));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            $username = $request->get('username');
            $password = $request->get('password');
            $checker = User::where([['username', $username], ['password', $password]])->first();
            if (count($checker) > 0) {
                return redirect()->back()->withCookie(Cookie::make('login_flag', 1, 1440))
                    ->withCookie(Cookie::make('user_name', $checker->name, 1440))
                    ->withCookie(Cookie::make('user_id', $checker->id, 1440));
            } else {
                Session::flash('message_type', 'danger');
                Session::flash('message_content', 'Tên đăng nhập hoặc mật khẩu không đúng!');
                return redirect()->back();
            }
        }
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function registerPost(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            array(
                'username' => 'min:4',
                'password' => 'min:6',
                're_password' => 'min:6',
                'phone_number' => 'digits_between:9,12'
            ),
            array(
                'username.min' => 'Tên đăng nhập phải có tối thiếu 3 ký tự!',
                'password.min' => 'Mật khẩu phải có tối thiếu 6 ký tự!',
                're_password.min' => 'Mật khẩu nhập lại phải có tối thiếu 6 ký tự!',
                'phone_number.digits_between' => 'Số điện thoại phải từ có ít nhất 9 số và nhiều nhất là 12 số!'
            ));
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        } else {
            if ($request->get('password') != $request->get('re_password')) {
                Session::flash('message_type', 'danger');
                Session::flash('message_content', 'Mật khẩu nhập lại không khớp!');
                return redirect()->back();
            } else if (count(User::where('username', $request->get('username'))->first()) > 0) {
                Session::flash('message_type', 'danger');
                Session::flash('message_content', 'Tên tài khoản đã tồn tại!');
                return redirect()->back();
            } else if (count(User::where('email', $request->get('email'))->first()) > 0) {
                Session::flash('message_type', 'danger');
                Session::flash('message_content', 'Email đăng ký đã được sử dụng!');
                return redirect()->back();
            } else {
                $new = new User();
                $new->username = $request->get('username');
                $new->password = $request->get('password');
                $new->email = $request->get('email');
                $new->address = $request->get('address');
                $new->name = $request->get('name');
                $new->phone = $request->get('phone_number');
                $new->birthday = $request->get('birthday');
                $new->status = 1;
                $new->save();
                Session::flash('message_type', 'success');
                Session::flash('message_content', 'Chào mừng ' . $new->name . ' đến với chúng tôi!');
                return redirect()->route('account.profile', ['id' => $new->id])->withCookie(Cookie::make('login_flag', 1, 1440))
                    ->withCookie(Cookie::make('user_name', $new->name, 1440))
                    ->withCookie(Cookie::make('user_id', $new->id, 1440))
                    ->withCookie(Cookie::make('login_alert', $new->name, 1440));
            }
        }
    }

    public function profile($user_id)
    {
        $user = User::find($user_id);
        return view('account.profile', ['user' => $user]);
    }

    public function edit($user_id)
    {
        $user = User::find($user_id);
        return view('account.edit', ['user' => $user]);
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->address = $request->address;
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->birthday = $request->birthday;
        $user->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Thông tin tài khoản đã được cập nhật thành công!');
        return redirect()->route('account.profile', ['user_id' => $id]);
    }

    public function changePassword($user_id)
    {
        return view('account.changepassword', ['user_id' => $user_id]);
    }

    public function updatePassword($id, Request $request)
    {
        $user = User::find($id);
        if ($request->old_password != $user->password) {
            Session::flash('message_type', 'danger');
            Session::flash('message_content', 'Bạn nhập không đúng mật khẩu!');
            return redirect()->back();
        } else if ($request->new_password != $request->re_password) {
            Session::flash('message_type', 'danger');
            Session::flash('message_content', 'Mật khẩu nhập lại không khớp!');
            return redirect()->back();
        } else {
            $user->password = $request->new_password;
            $user->save();
            Session::flash('message_type', 'success');
            Session::flash('message_content', 'Thay đổi mật khẩu thành công!');
            return redirect()->route('account.profile', ['user_id' => $id]);
        }
    }
}