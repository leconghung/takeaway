<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Menu;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    public function show($id)
    {
        $name = Category::find($id)->name;
        $categorys = Category::where(function ($query) use ($id) {
            $query->where('id', $id)
                ->orwhere('parent_id', $id);
        })->get();
        $categoryIds = array();
        foreach ($categorys as $cat) {
            $categoryIds[] = $cat->id;
        }
        $articles = Article::whereIn('category_id', $categoryIds)->where('status', 1)->orderBy('id', 'desc')->paginate(12);
        return view('category', ['name' => $name, 'articles' => $articles]);
    }
}
