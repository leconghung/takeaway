<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Menu;
use App\Online;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::where('status', 1)->get();
        $articles = Article::where(array(['status', 1], ['special_flag', 1]))->orderBy('id', 'desc')->limit(6)->get();
        $bestsellers = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty'))
            ->where('article.status', 1)
            ->groupBy('transaction.article_id')
            ->orderBy('total_qty', 'desc')
            ->limit(6)
            ->get();
        $online_number = Online::select(DB::raw('count(id) as total'))->where('expired_time', '<', date('m/d/Y H:i:s'))->first()->total;
        return view('home', array('categories' => $categories, 'articles' => $articles,
            'bestsellers' => $bestsellers, 'online_number' => $online_number));
    }
}
