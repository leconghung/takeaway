<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class HelperController extends Controller
{
    public function paymentTutorial()
    {
        return view('menu.payment-tutorial');
    }
}
