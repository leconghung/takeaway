<?php

namespace App\Http\Controllers;

use App\Article;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;

class CartController extends Controller
{
    public function index()
    {
        $results = array();
        $total = 0;
        $cartbag = Cookie::get('cartbag');
        if (count($cartbag) > 0) {
            foreach ($cartbag as $key => $value) {
                $new = array();
                $article = Article::find($key);
                if ($article) {
                    $new['id'] = $article->id;
                    $new['name'] = $article->title;
                    $new['image'] = $article->image;
                    $new['price'] = $article->new_price;
                    $new['qty'] = $value;
                    $results[] = $new;
                    $total += $new['price'] * $new['qty'];
                }
            }
        }
        return view('cart', ['results' => $results, 'total' => $total]);
    }

    public function add(Request $request)
    {
        if (Cookie::has('cartbag')) {
            $cartbag = Cookie::get('cartbag');
            $flag = false;
            foreach ($cartbag as $key => &$value) {
                if ($key == $request->pid) {
                    $value += $request->qty;
                    $flag = true;
                }
            }
            if (!$flag) {
                $cartbag[$request->pid] = $request->qty;
            }
            $bagCookie = Cookie::make('cartbag', $cartbag, 1440);
            return redirect()->route('cart')->withCookie($bagCookie);
        } else {
            $cartbag = array();
            $cartbag[$request->pid] = $request->qty;
            $bagCookie = Cookie::make('cartbag', $cartbag, 1440);
            return redirect()->route('cart')->withCookie($bagCookie);
        }
    }

    public function remove($id)
    {
        $cartbag = Cookie::get('cartbag');
        foreach ($cartbag as $key => $value) {
            if ($key == $id) {
                array_forget($cartbag, $key);
            }
        }
        $bagCookie = Cookie::make('cartbag', $cartbag, 1440);
        return redirect()->route('cart')->withCookie($bagCookie);
    }

    public function confirm()
    {
        $userId = Cookie::get('user_id');
        $user = User::find($userId);
        return view('confirm', ['user' => $user]);
    }

    public function place(Request $request)
    {
        $cartbag = Cookie::get('cartbag');
        foreach ($cartbag as $key => $value) {
            $new = new Transaction();
            $new->article_id = $key;
            $new->user_id = Cookie::has('user_id') ? Cookie::get('user_id') : 10;
            $new->name = $request->name;
            $new->email = $request->email;
            $new->qty = $value;
            $new->phone = $request->phone;
            $new->address = $request->address;
            $new->status = "waiting";
            $new->save();
        }
        $cookie = Cookie::forget('cartbag');
        return redirect()->route('thankyou')->withCookie($cookie);
    }

    public function thankyou()
    {
        return view('thankyou');
    }

    public function update(Request $request)
    {
        $cartbag = Cookie::get('cartbag');
        foreach($cartbag as $key => &$value){
            $value = $request->get('qty_' . $key);
        }
        $cookie = Cookie::make('cartbag', $cartbag, 1440);
        return redirect()->back()->withCookie($cookie);
    }
}
