<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use App\Article;
use App\Online;
use App\Rating;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    public function index()
    {
        return redirect()->route('admin.dashboard');
    }

    public function login()
    {
        return view('admin.login');
    }

    public function loginPost(Request $request)
    {
        $admin = AdminUser::where(['username' => $request->username, 'password' => $request->password])
            ->first();
        if (count($admin) == 0) {
            Session::flash('message_type', 'danger');
            Session::flash('message_content', 'Tên đăng nhập hoặc mật khẩu không đúng!');
            return redirect()->back();
        } else {
            return redirect()->route('admin.dashboard')->withCookie(Cookie::make('admin_flag', 1, 1440))
                ->withCookie(Cookie::make('admin_name', $admin->name, 1440))
                ->withCookie(Cookie::make('admin_role', $admin->role, 1440))
                ->withCookie(Cookie::make('admin_id', $admin->id, 1440));
        }
    }

    public function dashboard()
    {
        $totalUser = count(User::all());
        $totalArticle = count(Article::all());
        $totalOrder = count(Transaction::all());
        $totalRating = count(Rating::all());

        // Statistics
        $transactions = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty, sum(transaction.qty) * article.new_price as total_price'))
            ->where('transaction.status', 'success')
            ->groupBy('transaction.article_id')
            ->orderBy('total_qty', 'desc')
            ->get();
        $totalQty = $totalPrice = 0;
        foreach ($transactions as $transaction) {
            $totalQty += $transaction->total_qty;
            $totalPrice += $transaction->total_price;
        }

        // Revenue
        $revenueByDay = $revenueByWeek = $revenueByMonth = $revenueByYear = 0;

        $transactionsDay = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty, sum(transaction.qty) * article.new_price as total_price'))
            ->where('transaction.status', 'success')
            ->where('transaction.updated_at', '>=', date('Y-m-d 00:00:00'))
            ->where('transaction.updated_at', '<=', date('Y-m-d 23:59:59'))
            ->groupBy('transaction.article_id')
            ->get();
        foreach ($transactionsDay as $day) {
            $revenueByDay += $day->total_price;
        }
        $transactionsWeek = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty, sum(transaction.qty) * article.new_price as total_price'))
            ->where('transaction.status', 'success')
            ->where('transaction.updated_at', '>=', date('Y-m-d 00:00:00', strtotime('last monday')))
            ->where('transaction.updated_at', '<=', date('Y-m-d 23:59:59', strtotime('next sunday')))
            ->groupBy('transaction.article_id')
            ->get();
        foreach ($transactionsWeek as $week) {
            $revenueByWeek += $week->total_price;
        }
        $transactionsMonth = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty, sum(transaction.qty) * article.new_price as total_price'))
            ->where('transaction.status', 'success')
            ->where('transaction.updated_at', '>=', date('Y-m-d 00:00:00', strtotime('first day of this month')))
            ->where('transaction.updated_at', '<=', date('Y-m-d 23:59:59', strtotime('last day of this month')))
            ->groupBy('transaction.article_id')
            ->get();
        foreach ($transactionsMonth as $month) {
            $revenueByMonth += $month->total_price;
        }
        $transactionsYear = Transaction::join('article', 'transaction.article_id', '=', 'article.id')
            ->select('transaction.*', DB::raw('sum(transaction.qty) as total_qty, sum(transaction.qty) * article.new_price as total_price'))
            ->where('transaction.status', 'success')
            ->where('transaction.updated_at', '>=', date('Y-01-01 00:00:00'))
            ->where('transaction.updated_at', '<=', date('Y-12-31 23:59:59'))
            ->groupBy('transaction.article_id')
            ->get();
        foreach ($transactionsYear as $year) {
            $revenueByYear += $year->total_price;
        }

        // Invoice
        $invoiceDay = $invoiceWeek = $invoiceMonth = $invoiceYear = 0;
        $invoiceDay = Transaction::select(DB::raw('count(id) as total_invoice'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00'))
            ->where('created_at', '<=', date('Y-m-d 23:59:59'))
            ->first()->total_invoice;
        $invoiceWeek = Transaction::select(DB::raw('count(id) as total_invoice'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('last monday')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('next sunday')))
            ->first()->total_invoice;
        $invoiceMonth = Transaction::select(DB::raw('count(id) as total_invoice'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('first day of this month')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('last day of this month')))
            ->first()->total_invoice;
        $invoiceYear = Transaction::select(DB::raw('count(id) as total_invoice'))
            ->where('created_at', '>=', date('Y-01-01 00:00:00'))
            ->where('created_at', '<=', date('Y-12-31 23:59:59'))
            ->first()->total_invoice;

        // New Account
        $accountDay = $accountWeek = $accountMonth = $accountYear = 0;
        $accountDay = User::select(DB::raw('count(id) as total_user'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00'))
            ->where('created_at', '<=', date('Y-m-d 23:59:59'))
            ->first()->total_user;
        $accountWeek = User::select(DB::raw('count(id) as total_user'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('last monday')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('next sunday')))
            ->first()->total_user;
        $accountMonth = User::select(DB::raw('count(id) as total_user'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('first day of this month')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('last day of this month')))
            ->first()->total_user;
        $accountYear = User::select(DB::raw('count(id) as total_user'))
            ->where('created_at', '>=', date('Y-01-01 00:00:00'))
            ->where('created_at', '<=', date('Y-12-31 23:59:59'))
            ->first()->total_user;

        // New Product
        $productDay = $productWeek = $productMonth = $productYear = 0;
        $productDay = Article::select(DB::raw('count(id) as total_product'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00'))
            ->where('created_at', '<=', date('Y-m-d 23:59:59'))
            ->first()->total_product;
        $productWeek = Article::select(DB::raw('count(id) as total_product'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('last monday')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('next sunday')))
            ->first()->total_product;
        $productMonth = Article::select(DB::raw('count(id) as total_product'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('first day of this month')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('last day of this month')))
            ->first()->total_product;
        $productYear = Article::select(DB::raw('count(id) as total_product'))
            ->where('created_at', '>=', date('Y-01-01 00:00:00'))
            ->where('created_at', '<=', date('Y-12-31 23:59:59'))
            ->first()->total_product;

        // Customer online
        $onlineDay = $onlineWeek = $onlineMonth = $onlineYear = 0;
        $onlineDay = Online::select(DB::raw('count(id) as total_online'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00'))
            ->where('created_at', '<=', date('Y-m-d 23:59:59'))
            ->first()->total_online;
        $onlineWeek = Online::select(DB::raw('count(id) as total_online'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('last monday')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('next sunday')))
            ->first()->total_online;
        $onlineMonth = Online::select(DB::raw('count(id) as total_online'))
            ->where('created_at', '>=', date('Y-m-d 00:00:00', strtotime('first day of this month')))
            ->where('created_at', '<=', date('Y-m-d 23:59:59', strtotime('last day of this month')))
            ->first()->total_online;
        $onlineYear = Online::select(DB::raw('count(id) as total_online'))
            ->where('created_at', '>=', date('Y-01-01 00:00:00'))
            ->where('created_at', '<=', date('Y-12-31 23:59:59'))
            ->first()->total_online;

        return view('admin.dashboard',
            ['totalUser' => $totalUser, 'totalArticle' => $totalArticle, 'totalOrder' => $totalOrder, 'transactions' => $transactions,
                'totalRating' => $totalRating, 'totalQty' => $totalQty, 'totalPrice' => $totalPrice,
                'revenueByDay' => $revenueByDay, 'revenueByWeek' => $revenueByWeek, 'revenueByMonth' => $revenueByMonth, 'revenueByYear' => $revenueByYear,
                'invoiceDay' => $invoiceDay, 'invoiceWeek' => $invoiceWeek, 'invoiceMonth' => $invoiceMonth, 'invoiceYear' => $invoiceYear,
                'accountDay' => $accountDay, 'accountWeek' => $accountWeek, 'accountMonth' => $accountMonth, 'accountYear' => $accountYear,
                'productDay' => $productDay, 'productWeek' => $productWeek, 'productMonth' => $productMonth, 'productYear' => $productYear,
                'onlineDay' => $onlineDay, 'onlineWeek' => $onlineWeek, 'onlineMonth' => $onlineMonth, 'onlineYear' => $onlineYear,
            ]);
    }

    public function logout()
    {
        return redirect()->route('admin.login')
            ->withCookies([Cookie::forget('admin_flag'), Cookie::forget('admin_name'), Cookie::forget('admin_id')]);
    }
}
