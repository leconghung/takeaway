<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class AccountController extends Controller
{
    public function userIndex()
    {
        $users = User::orderBy('id','desc')->paginate(8);
        return view('admin.user.index', ['users' => $users]);
    }

    public function userAdd()
    {
        return view('admin.user.add');
    }

    public function userStore(Request $request)
    {
        $new = new User();
        $new->username = $request->username;
        $new->password = $request->password;
        $new->name = $request->name;
        $new->email = $request->email;
        $new->phone = $request->phone;
        $new->address = $request->address;
        $new->birthday = $request->birthday;
        $new->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Tạo tài khoản thành công!');
        return redirect()->route('admin.user.index');
    }

    public function userEdit($id)
    {
        $user = User::find($id);
        return view('admin.user.edit', ['user' => $user]);
    }

    public function userUpdate(Request $request)
    {
        $new = User::find($request->id);
        $new->username = $request->username;
        $new->password = $request->password;
        $new->name = $request->name;
        $new->email = $request->email;
        $new->phone = $request->phone;
        $new->address = $request->address;
        $new->birthday = $request->birthday;
        $new->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Những thay đổi cho tài khoản ' . $new->username . ' đã được cập nhật!');
        return redirect()->route('admin.user.index');
    }

    public function userDestroy($id)
    {
        User::destroy($id);
        Session::flash('message_type', 'danger');
        Session::flash('message_content', 'Xóa tài khoản thành công!');
        return redirect()->back();
    }

    public function memberIndex()
    {
        $members = AdminUser::orderBy('id', 'desc')->paginate(8);
        return view('admin.member.index', ['members' => $members]);
    }

    public function memberAdd()
    {
        return view('admin.member.add');
    }

    public function memberStore()
    {

    }

    public function memeberEdit()
    {
        return view('admin.memeber.edit');
    }

    public function memberUpdate()
    {

    }

    public function memberDestroy($id)
    {
        AdminUser::destroy($id);
        return redirect()->back();
    }
}
