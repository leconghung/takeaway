<?php

namespace App\Http\Controllers\Admin;

use App\AdminUser;
use App\Article;
use App\Category;
use App\Menu;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{
    public function articleIndex()
    {
        $articles = Article::orderBy('id', 'desc')->paginate(8);
        return view('admin.article.article-index', ['articles' => $articles]);
    }

    public function articleAdd()
    {
        $categories = Category::select('id', 'name')->where('status', 1)->get();
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }

        $authors = AdminUser::select('id', 'name')->where('status', 1)->get();
        $authorList = array();
        foreach ($authors as $author) {
            $authorList[$author->id] = $author->name;
        }

        return view('admin.article.article-add', ['categoryList' => $categoryList, 'authorList' => $authorList]);
    }

    public function articleStore(Request $request)
    {
        $article = new Article();
        $article->title = $request->title;
        $article->qty = $request->qty;
        $article->old_price = $request->old_price;
        $article->new_price = $request->new_price;
        $article->content = $request->get('content');
        $request->special_flag == 'on' ? $article->special_flag = 1 : $article->special_flag = 0;
        $article->category_id = $request->category_id;
        $article->author_id = $request->author_id;
        $article->status = $request->status;

        // Save image
        $destinationPath = 'images/article/';
        $extension = Input::file('image')->getClientOriginalExtension();
        $filename = date('Ymdhis') . '_' . Input::file('image')->getClientOriginalName();
        Input::file('image')->move($destinationPath, $filename);
        $article->image = $destinationPath . $filename;
        $article->save();

        // Notice
        Session::flash('message_type', 'success');
        Session::flash('message_content', $article->title . ' đã được tạo thành công!');
        return redirect()->route('admin.article.article.index');
    }

    public function articleEdit($id)
    {
        $categories = Category::select('id', 'name')->where('status', 1)->get();
        $categoryList = array();
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }

        $authors = AdminUser::select('id', 'name')->where('status', 1)->get();
        $authorList = array();
        foreach ($authors as $author) {
            $authorList[$author->id] = $author->name;
        }

        $article = Article::find($id);
        return view('admin.article.article-edit', ['article' => $article, 'categoryList' => $categoryList, 'authorList' => $authorList]);
    }

    public function articleUpdate(Request $request)
    {
        $article = Article::find($request->id);
        $article->title = $request->title;
        $article->qty = $request->qty;
        $article->old_price = $request->old_price;
        $article->new_price = $request->new_price;
        $article->content = $request->get('content');
        $request->special_flag == 'on' ? $article->special_flag = 1 : $article->special_flag = 0;
        $article->category_id = $request->category_id;
        $article->author_id = $request->author_id;
        $article->status = $request->status;

        // Save image
        if(Input::file('image')){
            $destinationPath = 'images/article/';
            $extension = Input::file('image')->getClientOriginalExtension();
            $filename = date('Ymdhis') . '_' . Input::file('image')->getClientOriginalName();
            Input::file('image')->move($destinationPath, $filename);
            $article->image = $destinationPath . $filename;
        }
        $article->save();

        // Notice
        Session::flash('message_type', 'success');
        Session::flash('message_content', ' Thông tin của sản phẩm ' . $article->title . ' đã được thay đổi thành công!');
        return redirect()->route('admin.article.article.index');
    }

    public function articleDestroy($id)
    {
        Article::destroy($id);
        Session::flash('message_type', 'danger');
        Session::flash('message_content', ' Sản phẫm có id ' . $id . ' đã được xóa!');
        return redirect()->back();
    }

    public function categoryIndex()
    {
        $categories = Category::orderBy('id', 'desc')->paginate(8);
        return view('admin.article.category-index', ['categories' => $categories]);
    }

    public function categoryAdd()
    {
        $categories = Category::select('id', 'name')->where(['status' => 1, 'level' => 1])->get();
        $categoryList = array();
        $categoryList['0'] = "Không";
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        return view('admin.article.category-add', ['categoryList' => $categoryList]);
    }

    public function categoryStore(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->level = $request->level;
        $category->parent_id = $request->parent_id;
        $request->has_children ? $category->has_children = 1 : $category->has_children = 0;
        $category->status = $request->status;
        $category->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Chuyên mục ' . $category->name . ' đã được tạo thành công!');
        return redirect()->route('admin.article.category.index');
    }

    public function categoryEdit($id)
    {
        $categoryModel = Category::find($id);
        $categories = Category::select('id', 'name')->where(['status' => 1, 'level' => 1])->get();
        $categoryList = array();
        $categoryList['0'] = "Không";
        foreach ($categories as $category) {
            $categoryList[$category->id] = $category->name;
        }
        return view('admin.article.category-edit', ['category' => $categoryModel, 'categoryList' => $categoryList]);
    }

    public function categoryUpdate(Request $request)
    {
        $category = Category::find($request->id);
        $category->name = $request->name;
        $category->level = $request->level;
        $category->parent_id = $request->parent_id;
        $request->has_children ? $category->has_children = 1 : $category->has_children = 0;
        $category->status = $request->status;
        $category->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Chuyên mục có id ' . $category->id . ' đã được cập nhật!');
        return redirect()->route('admin.article.category.index');
    }

    public function categoryDestroy($id)
    {
        Category::destroy($id);
        Session::flash('message_type', 'danger');
        Session::flash('message_content', 'Chuyên mục có id ' . $id . ' đã được xóa!');
        return redirect()->back();
    }

    public function menuIndex()
    {
        $menus = Menu::orderBy('id', 'desc')->paginate(10);
        return view('admin.article.menu-index', ['menus' => $menus]);
    }

    public function menuAdd()
    {
        return view('admin.article.menu-add');
    }

    public function menuStore(Request $request)
    {
        $menu = new Menu();
        $menu->name = $request->name;
        $menu->identifier = $request->identifier;
        $menu->position = $request->position;
        $menu->content = $request->get('content');
        $menu->status = $request->status;
        $menu->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Menu ' . $menu->name . ' đã được tạo thành công!');
        return redirect()->route('admin.article.menu.index');
    }

    public function menuEdit($id)
    {
        $menu = Menu::find($id);
        return view('admin.article.menu-edit', ['menu' => $menu]);
    }

    public function menuUpdate(Request $request)
    {
        $menu = Menu::find($request->id);
        $menu->name = $request->name;
        $menu->identifier = $request->identifier;
        $menu->position = $request->position;
        $menu->content = $request->get('content');
        $menu->status = $request->status;
        $menu->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Menu ' . $menu->name . ' đã được cập nhật thành công!');
        return redirect()->route('admin.article.menu.index');
    }

    public function menuDestroy($id)
    {
        Menu::destroy($id);
        Session::flash('message_type', 'danger');
        Session::flash('message_content', 'Menu có id ' . $id . ' đã được xóa!');
        return redirect()->back();
    }
}
