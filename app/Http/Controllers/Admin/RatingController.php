<?php

namespace App\Http\Controllers\Admin;

use App\Rating;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RatingController extends Controller
{
    public function index()
    {
        $ratings = Rating::orderBy('id', 'desc')->paginate(10);
        return view('admin.rating.index', ['ratings' => $ratings]);
    }

    public function changeStatus()
    {

    }

    public function add()
    {
        return view('admin.rating.add');
    }

    public function ratingStore(Request $request)
    {

    }

    public function edit($id)
    {
        return view('admin.rating.edit');
    }

    public function update(Request $request)
    {

    }

    public function destroy($id)
    {

    }
}
