<?php

namespace App\Http\Controllers\Admin;

use App\Transaction;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class OrderController extends Controller
{
    public function index()
    {
        $transactions = Transaction::orderBy('id', 'desc')->paginate(8);
        return view('admin.order.index', ['transactions' => $transactions]);
    }

    public function changeStatus($id, $status)
    {
        Transaction::where('id', $id)->update(['status' => $status]);
        return redirect()->back();
    }

    public function add()
    {
        return view('admin.order.add');
    }

    public function store(Request $request)
    {
        $transaction = New Transaction();
        $transaction->article_id = $request->article_id;
        $transaction->user_id = 10;
        $transaction->name = $request->name;
        $transaction->email = $request->email;
        $transaction->phone = $request->phone;
        $transaction->qty = $request->qty;
        $transaction->address = $request->address;
        $transaction->status = $request->status;
        $transaction->save();
        Session::flash('message_type', 'success');
        Session::flash('message_content', 'Đơn hàng có id ' . $transaction->id . ' đã được tạo thành công!');
        return redirect()->route('admin.order.index');
    }
}
