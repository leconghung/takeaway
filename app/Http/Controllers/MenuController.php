<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

use App\Http\Requests;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if($id == 1){
            return redirect()->to('/');
        }
        if($id == 2){
            return view('menu.introduction');
        }
        if($id == 3){
            return view('menu.menu');
        }
        if($id == 4){
            return view('menu.buy-tutorial');
        }
        if($id == 5){
            return view('menu.video');
        }
        if($id == 6){
            return view('menu.contact');
        }
        if($id == 7){
            return view('menu.support');
        }
        return view('construction');
    }
}
