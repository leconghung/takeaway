<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Rating;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Cookie;

class ArticleController extends Controller
{
    public function show($id)
    {
        $article = Article::find($id);
        $article->viewed = $article->viewed + 1;
        $article->save();
        return view('article', ['article' => $article]);
    }

    public function searchPost(Request $request)
    {
        return $this->search($request->searchKey, $request);
    }

    public function search($searchKey, Request $request)
    {
        $categories = Category::where('status', 1)->get();
        $categoryList = array();
        $categoryList[0] = 'Tất cả';
        foreach ($categories as $cat) {
            $categoryList[$cat->id] = $cat->name;
        }

        $categoryId = $sPrice = $ePrice = null;

        if ($request->has('advanced')) {
            $articles = Article::where(function ($query) use ($searchKey) {
                $query->where('title', 'like', '%' . $searchKey . '%');
//                    ->orWhere('content', 'like', '%' . $searchKey . '%');
            })->where(function ($query2) use ($request, &$sPrice, &$ePrice, &$categoryId) {
                $query2->where('status', 1);
                if ($request->start_price) {
                    $sPrice = $request->start_price;
                    $query2->where('new_price', '>=', $request->start_price);
                }
                if ($request->end_price) {
                    $ePrice = $request->end_price;
                    $query2->where('new_price', '<=', $request->end_price);
                }
                if ($request->category != 0) {
                    $categoryId = $request->category;
                    $catObj = Category::where('id', $request->category)->orWhere('parent_id', $request->category)->select('id')->get();
                    $carArr = array();
                    foreach ($catObj as $cat) {
                        $carArr[] = $cat->id;
                    }
                    $query2->whereIn('category_id', $carArr);
                }
            })
                ->orderBy('id', 'desc')
                ->paginate(12);
        } else {
            $articles = Article::where(function ($query) use ($searchKey) {
                $query->where('title', 'like', '%' . $searchKey . '%');
//                    ->orWhere('content', 'like', '%' . $searchKey . '%');
            })->where('status', 1)
                ->orderBy('id', 'desc')
                ->paginate(12);
        }
        return view('search', ['searchKey' => $searchKey, 'articles' => $articles, 'categoryList' => $categoryList,
            'categoryId' => $categoryId, 'start_price' => $sPrice, 'end_price' => $ePrice]);
    }

    public function vote($id, Request $request)
    {
        $userId = Cookie::get('user_id');
        $new = new Rating();
        $new->article_id = $id;
        $new->user_id = $userId;
        $new->rating = $request->point;
        $new->save();
        return redirect()->back();
    }
}
