<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/', 'HomeController@index')->middleware('online');
Route::get('menu/{id}', ['as' => 'menu.show', 'uses' => 'MenuController@show'])->middleware('online');
Route::get('category/{id}', ['as' => 'category.show', 'uses' => 'CategoryController@show'])->middleware('online');

Route::group(['prefix' => 'article'], function () {
    Route::get('{id}', ['as' => 'article.show', 'uses' => 'ArticleController@show']);
    Route::post('{id}/buy', ['as' => 'article.buy', 'uses' => 'ArticleController@buy']);
    Route::get('search/{searchKey}', ['as' => 'article.search', 'uses' => 'ArticleController@search']);
    Route::post('searchPost', ['as' => 'article.searchPost', 'uses' => 'ArticleController@searchPost']);
    Route::post('{id}/vote', ['as' => 'article.vote', 'uses' => 'ArticleController@vote']);
});

Route::group(['prefix' => 'account', 'middleware' => 'online'], function () {
    Route::get('register', ['as' => 'account.register', 'uses' => 'AccountController@create'])->middleware('sign');
    Route::get('login', ['as' => 'account.login', 'uses' => 'AccountController@login'])->middleware('sign');
    Route::get('logout', ['as' => 'account.logout', 'uses' => 'AccountController@logout']);
    Route::post('loginPost', ['as' => 'account.loginPost', 'uses' => "AccountController@loginPost"]);
    Route::post('registerPost', ['as' => 'account.registerPost', 'uses' => "AccountController@registerPost"]);
    Route::get('profile/{user_id}', ['as' => 'account.profile', 'uses' => 'AccountController@profile'])->middleware('account');
    Route::get('{user_id}/changePassword', ['as' => 'account.changePassword', 'uses' => 'AccountController@changePassword'])->middleware('account');
    Route::post('{user_id}/updatePassword', ['as' => 'account.updatePassword', 'uses' => 'AccountController@updatePassword'])->middleware('account');
    Route::get('{user_id}/edit', ['as' => 'account.edit', 'uses' => 'AccountController@edit'])->middleware('account');
    Route::post('{user_id}/update', ['as' => 'account.update', 'uses' => 'AccountController@update'])->middleware('account');
});

Route::group(['prefix' => 'cart', 'middleware' => 'online'], function () {
    Route::get('/', ['as' => 'cart', 'uses' => "CartController@index"]);
    Route::post('add', ['as' => 'cart.add', 'uses' => "CartController@add"]);
    Route::get('{id}/remove', ['as' => 'cart.remove', 'uses' => "CartController@remove"]);
    Route::get('confirm', ['as' => 'cart.confirm', 'uses' => "CartController@confirm"]);
    Route::post('place', ['as' => 'cart.place', 'uses' => "CartController@place"]);
    Route::post('update', ['as' => 'cart.update', 'uses' => "CartController@update"]);
});

Route::get('thankyou', ['as' => 'thankyou', 'uses' => "CartController@thankyou"])->middleware('online');
Route::get('paymenttutorial', ['as' => 'paymenttutorial', 'uses' => 'HelperController@paymentTutorial'])->middleware('online');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'Admin\AdminController@index']);
    Route::get('dashboard', ['as' => 'admin.dashboard', 'uses' => 'Admin\AdminController@dashboard'])->middleware('admin');
    Route::get('login', ['as' => 'admin.login', 'uses' => 'Admin\AdminController@login'])->middleware('adminsign');
    Route::get('logout', ['as' => 'admin.logout', 'uses' => 'Admin\AdminController@logout']);
    Route::post('loginPost', ['as' => 'admin.loginPost', 'uses' => 'Admin\AdminController@loginPost']);

    Route::group(['prefix' => 'user', 'middleware' => 'admin'], function () {
        Route::get('index', ['as' => 'admin.user.index', 'uses' => 'Admin\AccountController@userIndex']);
        Route::get('add', ['as' => 'admin.user.add', 'uses' => 'Admin\AccountController@userAdd']);
        Route::post('store', ['as' => 'admin.user.store', 'uses' => 'Admin\AccountController@userStore']);
        Route::get('{id}/edit', ['as' => 'admin.user.edit', 'uses' => 'Admin\AccountController@userEdit']);
        Route::post('update', ['as' => 'admin.user.update', 'uses' => 'Admin\AccountController@userUpdate']);
        Route::get('{id}/destroy', ['as' => 'admin.user.destroy', 'uses' => 'Admin\AccountController@userDestroy']);
    });

    Route::group(['prefix' => 'member', 'middleware' => 'admin'], function () {
        Route::get('index', ['as' => 'admin.member.index', 'uses' => 'Admin\AccountController@memberIndex']);
        Route::get('add', ['as' => 'admin.member.add', 'uses' => 'Admin\AccountController@memberAdd']);
        Route::post('store', ['as' => 'admin.member.store', 'uses' => 'Admin\AccountController@memberStore']);
        Route::get('{id}/edit', ['as' => 'admin.member.edit', 'uses' => 'Admin\AccountController@memberEdit']);
        Route::post('update', ['as' => 'admin.member.update', 'uses' => 'Admin\AccountController@memberUpdate']);
        Route::get('{id}/destroy', ['as' => 'admin.member.destroy', 'uses' => 'Admin\AccountController@memberDestroy']);
    });

    Route::group(['prefix' => 'article', 'middleware' => 'admin'], function () {
        Route::group(['prefix' => 'article', 'middleware' => 'admin'], function () {
            Route::get('index', ['as' => 'admin.article.article.index', 'uses' => 'Admin\ArticleController@articleIndex']);
            Route::get('add', ['as' => 'admin.article.article.add', 'uses' => 'Admin\ArticleController@articleAdd']);
            Route::post('store', ['as' => 'admin.article.article.store', 'uses' => 'Admin\ArticleController@articleStore']);
            Route::get('{id}/edit', ['as' => 'admin.article.article.edit', 'uses' => 'Admin\ArticleController@articleEdit']);
            Route::post('update', ['as' => 'admin.article.article.update', 'uses' => 'Admin\ArticleController@articleUpdate']);
            Route::get('{id}/destroy', ['as' => 'admin.article.article.destroy', 'uses' => 'Admin\ArticleController@articleDestroy']);
        });

        Route::group(['prefix' => 'category', 'middleware' => 'admin'], function () {
            Route::get('index', ['as' => 'admin.article.category.index', 'uses' => 'Admin\ArticleController@categoryIndex']);
            Route::get('add', ['as' => 'admin.article.category.add', 'uses' => 'Admin\ArticleController@categoryAdd']);
            Route::post('store', ['as' => 'admin.article.category.store', 'uses' => 'Admin\ArticleController@categoryStore']);
            Route::get('{id}/edit', ['as' => 'admin.article.category.edit', 'uses' => 'Admin\ArticleController@categoryEdit']);
            Route::post('update', ['as' => 'admin.article.category.update', 'uses' => 'Admin\ArticleController@categoryUpdate']);
            Route::get('{id}/destroy', ['as' => 'admin.article.category.destroy', 'uses' => 'Admin\ArticleController@categoryDestroy']);
        });

        Route::group(['prefix' => 'menu', 'middleware' => 'admin'], function () {
            Route::get('index', ['as' => 'admin.article.menu.index', 'uses' => 'Admin\ArticleController@menuIndex']);
            Route::get('add', ['as' => 'admin.article.menu.add', 'uses' => 'Admin\ArticleController@menuAdd']);
            Route::post('store', ['as' => 'admin.article.menu.store', 'uses' => 'Admin\ArticleController@menuStore']);
            Route::get('{id}/edit', ['as' => 'admin.article.menu.edit', 'uses' => 'Admin\ArticleController@menuEdit']);
            Route::post('update', ['as' => 'admin.article.menu.update', 'uses' => 'Admin\ArticleController@menuUpdate']);
            Route::get('{id}/destroy', ['as' => 'admin.article.menu.destroy', 'uses' => 'Admin\ArticleController@menuDestroy']);
        });
    });

    Route::group(['prefix' => 'order', 'middleware' => 'admin'], function () {
        Route::get('index', ['as' => 'admin.order.index', 'uses' => 'Admin\OrderController@index']);
        Route::get('add', ['as' => 'admin.order.add', 'uses' => 'Admin\OrderController@add']);
        Route::post('store', ['as' => 'admin.order.store', 'uses' => 'Admin\OrderController@store']);
        Route::get('{id}/changeStatus/{status}', ['as' => 'admin.order.changeStatus', 'uses' => 'Admin\OrderController@changeStatus']);
    });

    Route::group(['prefix' => 'rating', 'middleware' => 'admin'], function () {
        Route::get('index', ['as' => 'admin.rating.index', 'uses' => 'Admin\RatingController@index']);
    });
});
