<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;

class Article extends Model
{
    protected $table = 'article';

    public function getRatingPercent()
    {
        $rating = Rating::select(DB::raw('count(id) as number, sum(rating) as total'))
            ->groupBy('article_id')->where('article_id', $this->id)->first();
        $result = 0;
        if (count($rating) > 0) {
            $result = round(($rating->total / $rating->number) / 5 * 100);
        }
        return $result;
    }

    public function getRatingNumber()
    {
        $rating = Rating::select(DB::raw('count(id) as number, sum(rating) as total'))
            ->groupBy('article_id')->where('article_id', $this->id)->first();
        $result = 0;
        if (count($rating) > 0) {
            $result = round($rating->total / $rating->number);
        }
        return $result;
    }

    public function canRate()
    {
        $flag = false;
        if (Cookie::has('login_flag')) {
            $user_id = Cookie::get('user_id');
            $rating = Rating::where('user_id', $user_id)->where('article_id', $this->id)->get();
            if (count($rating) == 0) {
                $flag = true;
            }
        }
        return $flag;
    }

    public function getRateOfCurrentUser()
    {
        $userId = Cookie::get('user_id');
        $rating = Rating::where('user_id', $userId)->where('article_id', $this->id)->first();
        $point = $rating->rating;
        return $point;
    }

    public function getCategoryName()
    {
        return Category::find($this->category_id)->name;
    }

    public function getAuthorName()
    {
        return User::find($this->author_id)->name;
    }
}

