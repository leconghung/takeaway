<?php

namespace App\Providers;

use App\Article;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\ServiceProvider;

class CartProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.elements.header', function ($view) {
            $totalItem = $totalPrice = 0;
            if (Cookie::has('cartbag')) {
                $cartbag = Cookie::get('cartbag');
                foreach ($cartbag as $key => $value) {
                    $article = Article::find($key);
                    if ($article) {
                        $totalItem += $value;
                        $totalPrice += ($article->new_price) * $value;
                    }
                }
            }
            $view->with(['totalItem' => $totalItem, 'totalPrice' => $totalPrice]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
