<?php

namespace App\Providers;

use App\Menu;
use Illuminate\Support\ServiceProvider;

class MenuProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.elements.menu', function($view){
            $menus = Menu::where('status', 1)->orderBy('position')->get();
            $view->with('menus', $menus);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
