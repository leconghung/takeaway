<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    protected $table = 'admin_user';

    public function getAdminRoleName()
    {
        $name = '';
        switch ($this->role) {
            case 1 :
                $name = 'Supper Admin'; break;
            default :
                $name = "Default Admin";
        }
        return $name;
    }
}
