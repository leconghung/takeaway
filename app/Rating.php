<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $table = "rating";

    public function article()
    {
        $this->belongsTo('App\Article', 'article_id', 'id');
    }

    public function user()
    {
        $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getArticleName()
    {
        return Article::find($this->article_id)->title;
    }

    public function getUserName()
    {
        return User::find($this->user_id)->name;
    }
}
