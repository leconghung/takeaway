@extends('layouts.master')
@section('title')
    Take Away
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{!! asset('css/jquery.bxslider.css') !!}">
@endsection
@section('content')

    {{--Slider and Menu--}}
    <div id="content">
        @include('layouts.elements.category-list')
        @include('layouts.elements.flexslider')
    </div>
    <div class="clr"></div>

    @include('layouts.elements.bestseller')

    @include('layouts.elements.promotion')

    <div id="main_block">
        @include('layouts.elements.featured-product')

        <div class="right">
            @include('layouts.elements.supporter')
            <br>
            @include('layouts.elements.online_number')
            <br>
            @include('layouts.elements.video-right')
        </div>
    </div>
@endsection