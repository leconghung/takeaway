@extends('layouts.master')
@section('title')
    Giỏ hàng
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="cartpage">
        <div class="container-fluid">

            @if(count($results) == 0)
                <p class="lead text-danger">Giỏ hàng của bạn rỗng!</p>
            @else
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'cart.update'
                ]) !!}
                <table class="table table-bordered">
                    <tr class="text-danger">
                        <td>Tên sản phẩm</td>
                        <td>Hình ảnh</td>
                        <td>Giá bán</td>
                        <td width="100px">Số lượng</td>
                        <td>Tổng</td>
                        <td align="center">Xóa</td>
                    </tr>
                    @foreach($results as $result)
                        <tr>
                            <td>
                                <a target="_blank"
                                   href="{!! route('article.show', ['id' => $result['id'] ]) !!}">{!! $result['name'] !!}</a>
                            </td>
                            <td><a href="{!! route('article.show', ['id' => $result['id'] ]) !!}"><img
                                            src="{!! asset($result['image']) !!}" alt=""
                                            style="max-width: 50px; height: auto"></a></td>
                            <td>{!! number_format($result['price'], 0, '', '.') !!} đ</td>
                            <td width="100px">{!! Form::number('qty_' . $result['id'], $result['qty'], ['class' => 'form-control', 'min' => 1, 'max' => 10]) !!}</td>
                            <td>{!! number_format($result['price'] * $result['qty'], 0, '', '.') !!} đ</td>
                            <td align="center" width="50px"><a
                                        href="{!! route('cart.remove',['id' => $result['id']]) !!}"
                                        title="Xóa khỏi giỏ hàng"><span
                                            class="glyphicon glyphicon-remove text-danger"></span></a></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" class="text-right text-danger">Chi phí phải trả (Chưa tính tiền vận chuyển nếu có):</td>
                        <td class="text-danger">{!! number_format($total, 0, '', '.') !!} đ</td>
                    </tr>
                </table>
                <div class="row">
                    {{--@if(\Illuminate\Support\Facades\Cookie::has('login_flag'))--}}
                        <div class="col-sm-2 col-sm-offset-8">
                            {!! Form::submit('Cập nhật giỏ hàng', ['class' => 'btn btn-info']) !!}
                        </div>
                        <div class="col-sm-2"><a class="btn btn-danger"
                                                 href="{!! route('cart.confirm') !!}">Bước tiếp theo <span
                                        class="glyphicon glyphicon-menu-right"></span></a></div>
                        </div>
                {{--@else--}}
                    {{--<div class="col-sm-6 col-sm-offset-6 text-right">Bạn cần <a--}}
                                {{--href="{!! route('account.login') !!}">đăng nhập</a> để thực hiện--}}
                        {{--bước tiếp theo!--}}
                        {{--@endif--}}
                {{--</div>--}}
                {!! Form::close() !!}
            @endif
        </div>
    </div>
@endsection