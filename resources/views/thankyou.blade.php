@extends('layouts.master')
@section('title')
    Cảm ơn bạn!
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="cartpage">
        <div class="container-fluid">
            <div class="row">
                <p class="lead text-danger">
                    Cảm ơn bạn đã đặt hàng của chúng tôi <br/>
                    Chúng tôi sẽ chuyển hàng tới bạn sớm nhất có thể! <br/>
                <ul class="lead text-info">
                    <li><u>Thời gian chuyển hàng</u>: Ngay sau khi được xác nhận của hỗ trợ viên. (Sau khi bạn đặt hàng,
                        chúng tôi sẽ gọi điện cho bạn để xác nhận lại một lần nữa!)
                    </li>
                    <li><u>Chi phí chuyển hàng</u>: Miễn phí đối với các vùng nội thành Hà nội, với các vùng ngoại
                        +20.000 tiền vận chuyển
                    </li>
                    <li><u>Hình thức thanh toán</u>: Trả tiền sau khi nhận được hàng</li>
                </ul>
                <span class="lead text-muted">Mọi thắc mắc vui lòng liên hệ: Lê Thị Nga - ĐT: 01666516283 - Email: lengatkt56@gmail
                    .com</span>
                </p>
            </div>
        </div>
    </div>
@endsection