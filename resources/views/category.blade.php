@extends('layouts.master')
@section('title')
    {!! $name !!}
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/article.css') }}" type="text/css">
@endsection
@section('content')
    <div id="product-list">
        <div class="container-fluid">
            <p class="lead text-warning">Danh mục sản phẩm của chuyên mục: <span
                        class="text-danger">{!! $name !!}</span></p>
            @if(count($articles) == 0)
                <p class="lead text-info">Hiện tại chưa có sản phẩm nào!</p>
            @endif
            <div class="row">
                @foreach($articles as $article)
                    <div class="col-sm-3">
                        <div class="product-item">
                            <div class="title"><a
                                        href="{!! route('article.show', ['id' => $article->id]) !!}">{!! $article->title !!}</a>
                            </div>
                            <div class="image"><a href="{!! route('article.show', ['id' => $article->id]) !!}"><img
                                            src="{!! asset($article->image) !!}"
                                            alt="{!! $article->title !!}"
                                            title="{!! $article->title !!}"></a>
                            </div>
                            <div class="price">
                                <span class="new-price">{!! number_format($article->new_price, 0, '', '.') !!} đ</span>
                                @if($article->old_price != null)
                                    <span class="old-price">{!! number_format($article->old_price, 0, '', '.') !!}
                                        đ</span>
                                @endif
                            </div>
                            <div class="rating">
                                @for($i = 1; $i <= $article->getRatingNumber(); $i++ )
                                    <img src="{!! asset('images/star-on.png') !!}" alt="1" title="good">
                                @endfor
                            </div>
                            <div class="view-buy">
                                <div class="view pull-left">
                                    <label for="">Lượt xem: {!! $article->viewed !!}</label>
                                </div>
                                <div class="buy pull-right">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'url' => route('cart.add')
                                    ]) !!}
                                    @if($article->qty > 0)
                                        {!! Form::submit('Mua ngay', ['class' => 'form-control btn-primary']) !!}
                                    @else
                                        <a href="{!! route('article.show', ['id' => $article->id]) !!}"
                                           class="btn btn-warning btn-sm">Xem</a>
                                    @endif
                                    {!! Form::hidden('pid', $article->id) !!}
                                    {!! Form::hidden('qty', 1) !!}
                                    {!! Form::close() !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {!! $articles->links() !!}
        </div>
    </div>
@endsection