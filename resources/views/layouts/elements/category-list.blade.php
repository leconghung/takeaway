<div id="content-left">
    <div id="top-menu">
        <ul>
            <li>DANH MỤC</li>
        </ul>
    </div>
    <div id="menu-left">
        {{--Just used for category menu with 2 level--}}
        <ul>
            @foreach($categories as $category)
                @if($category->level == 1)
                    <li>
                        <a href="{{ route('category.show', ['id' => $category->id]) }}">{{ $category->name }}</a>
                    </li>
                @endif
                @if($category->has_children == 1)
                    <ul class="submenu">
                        @foreach($categories as $sub_category)
                            @if($sub_category->parent_id == $category->id)
                                <li>
                                    <a href="{{ route('category.show', ['id' => $sub_category->id]) }}">{{ $sub_category->name }}</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif
            @endforeach
        </ul>
    </div>
</div>