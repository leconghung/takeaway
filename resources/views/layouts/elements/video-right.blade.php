<br>
<p class="text-danger bold">Cách làm cà phê đá xay - Cookie Cream cực ngon</p>
<video width="250" height="auto" controls style="border: 1px solid #ccc;">
    <source src="{!! asset('videos/Cookie_Cream.mp4') !!}" type="video/mp4">
    Your browser does not support the video tag.
</video>
<br><br><br>
<p class="text-danger bold">Hướng dẫn làm Panna Cotta</p>
<video width="250" height="auto" controls style="border: 1px solid #ccc;">
    <source src="{!! asset('videos/Panna_Cotta.mp4') !!}" type="video/mp4">
    Your browser does not support the video tag.
</video>
<p class="mar-top-10 text-right">Xem thêm video <a href="{!! route('menu.show', ['id' => 5]) !!}">tại đây</a></p>