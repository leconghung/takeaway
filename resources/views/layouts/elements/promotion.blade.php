{{--Promotion--}}
<section class="promotion">
    <div class="box">
        <div class="row">
            <div class="colum">
                <div class="reassurances-left">
							<span class="icon-box">
								<img src="images/home_blockhtml_icon_1.png">
							</span>

                    <div class="media-body">
                        <h4>Miễn phí vận chuyển</h4>

                        <p>Trong vòng 5km</p>
                        <a href="javascript:void(0)" class="apreassurances arrow" data-toggle="modal"
                           data-target="#myModal1"></a>
                    </div>
                </div>
            </div>
            <div class="colum">
                <div class="reassurances-left">
							<span class="icon-box">
								<img src="images/home_blockhtml_icon_2.png">
							</span>

                    <div class="media-body">
                        <h4>Hỗ trợ khách hàng</h4>

                        <p>24/24h Tất cả các ngày</p>
                        <a href="javascript:void(0)" class="apreassurances arrow" data-toggle="modal"
                           data-target="#myModal2"></a>
                    </div>
                </div>
            </div>
            <div class="colum">
                <div class="reassurances-left">
							<span class="icon-box">
								<img src="images/home_blockhtml_icon_3.png">
							</span>

                    <div class="media-body">
                        <h4>Mua hàng tiện lợi</h4>

                        <p>Nhấn và chọn</p>
                        <a href="javascript:void(0)" class="apreassurances arrow" data-toggle="modal"
                           data-target="#myModal3"></a>
                    </div>
                </div>
            </div>
            <div class="colum">
                <div class="reassurances-left">
							<span class="icon-box">
								<img src="images/home_blockhtml_icon_4.png">
							</span>

                    <div class="media-body">
                        <h4>Giá cả hấp dẫn</h4>

                        <p>Giảm tới 30%</p>
                        <a href="javascript:void(0)" class="apreassurances arrow" data-toggle="modal"
                           data-target="#myModal4"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>