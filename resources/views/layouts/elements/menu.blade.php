<div id="menu">
    <ul>
        {{--<img alt="" src="{{ asset('images/home.png') }}"/>--}}
        @foreach($menus as $menu)
            <li><a href="{{ route('menu.show', array('id' => $menu->id)) }}">{{ $menu->name }}</a></li>
        @endforeach
    </ul>
</div>