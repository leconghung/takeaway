<div class="footer-container">
    <section id="footercenter" class="footer-center">
        <div class="container-footer">
            <div class="footer">
                <div class="footer-sologan">
                    <div class="block_aboutshop block">
                        <div class="block_content">
                            <div class="about">
                                "Take away-sự tin cậy và nhanh chóng cho khách hàng. Chúng tôi luôn mang đến
                                những mặt hàng chất lượng và thơm ngon. Hãy tin tưởng chúng tôi."
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="colum1">
                        <div class="footer-block block" id="1">
                            <h4 class="title_block">
                                <span>Tài khoản</span>
                            </h4>

                            <div class="block_content" style>
                                <ul class="toggle list-group">
                                    @if(!\Illuminate\Support\Facades\Cookie::has('login_flag'))
                                        <li class="item"><a href="{!! route('account.register') !!}" title="Đăng kí">Đăng
                                                kí</a></li>
                                        <li class="item"><a href="{!! route('account.login') !!}" title="Đăng nhập">Đăng
                                                nhập</a></li>
                                    @endif
                                    <li class="item"><a href="{!! route('cart') !!}" title="Giỏ hàng">Giỏ hàng</a></li>
                                    <li class="item"><a href="{!! route('paymenttutorial') !!}" title="Hướng dẫn thanh toán">Hướng dẫn thanh
                                            toán</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="colum1">
                        <div class="footer-block block" id="2">
                            <h4 class="title_block">
                                <span>Take away</span>
                            </h4>

                            <div class="block_content" style>
                                <ul class="toggle list-group">
                                    <li class="item"><a href="{!! url('/') !!}" title="Trang chủ">Trang chủ</a></li>
                                    <li class="item"><a href="{!! url('menu/2') !!}" title="Giới thiệu">Giới thiệu</a></li>
                                    <li class="item"><a href="{!! url('menu/4') !!}" title="Hướng dẫn mua hàng">Hướng dẫn mua
                                            hàng</a>
                                    </li>
                                    <li class="item"><a href="{!! url()->to('menu/7') !!}" title="Hỗ trợ">Hỗ trợ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="colum1">
                        <div class="footer-block block contact">
                            <h4 class="title_block">
                                <span>Liên hệ</span>
                            </h4>

                            <div class="block_content" style>
                                <ul style>
                                    <li class="icon">Di động: 01666.516.283</li>
                                    <li class="icon"><span>Số 23 Xuân Thủy- Cầu Giấy- Hà Nội</span>
                                    </li>
                                    <li class="icon">Email:<a
                                                href="mailto:lengatkt56@gmail.com">lengatkt56@gmail.com</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="clearboth" style="clear: both;"></div>
                </div>
            </div>
        </div>
    </section>
</div>