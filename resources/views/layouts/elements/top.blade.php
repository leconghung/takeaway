<div id="top">
    <div id="top-left">
        <ul>
            <li><a href="#"><img src="{{ asset('images/google.png') }}"/></a></li>
            <li><a href="#"><img src="{{ asset('images/facebook.png') }}"/></a></li>
            <li><a href="#"><img src="{{ asset('images/skype.png') }}"/></a></li>
            <li><a href="#"><img src="{{ asset('images/gmail.png') }}"/></a></li>
        </ul>
    </div>
    <div id="top-right">
        <ul>
            <li><img src="{{ asset('images/thanhtoan.png') }}"/></li>
            <li>|</li>
            <li><a href="{!! route('paymenttutorial') !!}">Hướng dẫn thanh toán</a></li>
            <li>|</li>
            @if(\Illuminate\Support\Facades\Cookie::get('login_flag'))
                <li>
                    <a href="{!! route('account.profile', ['user_id' => \Illuminate\Support\Facades\Cookie::get('user_id')]) !!}">Chào, {{\Illuminate\Support\Facades\Cookie::get('user_name') }}</a>
                </li>
                <li>|</li>
                <li><a href="{{ route('account.logout') }}">Đăng xuất</a></li>
            @else
                <li>
                    <a href="{{ route('account.login') }}">Đăng nhập</a>
                </li>
                <li>|</li>
                <li><a href="{{ route('account.register') }}">Đăng kí</a></li>
            @endif
        </ul>
    </div>
</div>