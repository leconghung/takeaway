<div id="header">
    <div style="float:left; width: 250px; height: 170px; margin: 10px 0 0 35px;">
        <a href="{{ url('/') }}"><img src="{{ asset('images/logo.png') }}"></a>
    </div>
    <div style="float:right; width: 400px; height: 93px;">
        <div id="cartbag">
            <div class="pull-right">
                <a href="{!! route('cart') !!}"><img src="{{ asset('images/giohang.png') }}"/></a>
                <span class="badge">{!! $totalItem !!}</span> <span class="text-danger"><a
                            href="{!! route('cart') !!}">sản phẩm</a></span> | <span
                        class="text-danger bold">{!! number_format($totalPrice, 0, '', '.') !!}</span> <span
                        class="text-danger">đ</span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div style="float:right; width: 400px; height: 60px;">
        <div id="phone">
            Call: 01.666.516.283
        </div>
        <div id="search-box">
            {!! Form::open([
                'method' => 'post',
                'route' => 'article.searchPost',
                'id' => 'searchForm'
            ]) !!}
            <div class="form-group">
                <div class="input-group">
                    {!! Form::text('searchKey', "", ['class' => 'form-control', 'placeholder' => 'Nội dung tìm kiếm']) !!}
                    <div onclick="jQuery('#searchForm').submit() "
                         class="input-group-addon" style="cursor: pointer;"><span title="Tìm kiếm nâng cao"
                                                                                  class="glyphicon glyphicon-search"></span>
                    </div>
                </div>
            </div>
            <div class="clearboth"></div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

@section('body.js')
    <script type="text/javascript">

    </script>
@endsection
