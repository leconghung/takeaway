{{--Bestseller--}}
<div id="products_bestsell">
    <div id="productslistbestsell" class="products_block block">
        <h4 class="page-subheading">Bán chạy nhất</h4>
    </div>

    <div class="row">
        <ul class="bxslider">
            @foreach($bestsellers as $bestseller)
                <li>
                    {{--<div class="col-sm-3">--}}
                        <div class="product-item">
                            <div class="title"><a
                                        href="{!! route('article.show', ['id' => $bestseller->article->id]) !!}">{!! $bestseller->article->title !!}</a>
                            </div>
                            <div class="image"><a
                                        href="{!! route('article.show', ['id' => $bestseller->article->id]) !!}"><img
                                            src="{!! asset($bestseller->article->image) !!}"
                                            alt="{!! $bestseller->article->title !!}"
                                            title="{!! $bestseller->article->title !!}"></a>
                            </div>
                            <div class="price">
                            <span class="new-price">{!! number_format($bestseller->article->new_price, 0, '', '.') !!}
                                đ</span>
                                @if($bestseller->article->old_price != null)
                                    <span class="old-price">{!! number_format($bestseller->article->old_price, 0, '', '.') !!}
                                        đ</span>
                                @endif
                            </div>
                            <div class="rating">
                                @for($i = 1; $i <= $bestseller->article->getRatingNumber(); $i++ )
                                    <img src="{!! asset('images/star-on.png') !!}" alt="1" title="good">
                                @endfor
                            </div>
                            <div class="view-buy">
                                <div class="view pull-left">
                                    <label for="">Lượt xem: {!! $bestseller->article->viewed !!}</label>
                                </div>
                                <div class="buy pull-right">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'url' => route('cart.add')
                                    ]) !!}
                                    @if($bestseller->article->qty > 0)
                                        {!! Form::submit('Mua ngay', ['class' => 'form-control btn-primary']) !!}
                                    @else
                                        <a href="{!! route('article.show', ['id' => $bestseller->article->id]) !!}"
                                           class="btn btn-warning btn-sm">Xem</a>
                                    @endif
                                    {!! Form::hidden('pid', $bestseller->article->id) !!}
                                    {!! Form::hidden('qty', 1) !!}
                                    {!! Form::close() !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    {{--</div>--}}
                </li>
            @endforeach
        </ul>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="{!! asset('js/jquery.bxslider.min.js') !!}"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.bxslider').bxSlider({
            pager: false,
            minSlides: 3,
            maxSlides: 4,
            slideWidth: 240,
            slideMargin: 12
        });
    });
</script>