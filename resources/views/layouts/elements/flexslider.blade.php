<div id="content-nav">
    <section class="slider">
        <div class="flexslider">
            <ul class="slides">
                <li>
                    <img src="images/kitchen_adventurer_cheesecake_brownie.jpg"/>
                </li>
                <li>
                    <img src="images/kitchen_adventurer_lemon.jpg"/>
                </li>
                <li>
                    <img src="images/kitchen_adventurer_donut.jpg"/>
                </li>
                <li>
                    <img src="images/kitchen_adventurer_caramel.jpg"/>
                </li>
            </ul>
        </div>
    </section>
</div>
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<!-- FlexSlider -->
<script defer src="{!! asset('js/jquery.flexslider.js') !!}"></script>
<script type="text/javascript">
//    $(function () {
//        SyntaxHighlighter.all();
//    });
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "slide",
            controlNav: false,
        });
    });
</script>