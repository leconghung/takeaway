<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/supporter.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
    <script src="{!! asset('js/jquery-2.2.4.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
    @yield('head')
</head>
<body>
<div id="page">
    @include('layouts.elements.top')
    @include('layouts.elements.header')
    @include('layouts.elements.menu')
    @yield('content')
    @include('layouts.elements.footer')
</div>
@include('supporter.alert')
@yield('body.js')
</body>
</html>