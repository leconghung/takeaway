@extends('layouts.master')
@section('title')
    Thông tin cá nhân
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="profile">
        <div class="container-fluid">
            <p class="lead text-warning">Thông tin cá nhân của: <span class="text-danger">{!! $user->name !!}</span></p>

            @include('supporter.alert')

            <div class="row">
                <div class="col-sm-3">Tên đăng nhập:</div>
                <div class="col-sm-9 text-danger">{!! $user->username !!}</div>
                <div class="col-sm-3">Mật khẩu:</div>
                <div class="col-sm-9 text-danger underline"><a
                            href="{!! route('account.changePassword', ['user_id' => \Illuminate\Support\Facades\Cookie::get('user_id')]) !!}">Đổi
                        mật khẩu</a></div>
                <div class="col-sm-3">Email:</div>
                <div class="col-sm-9 text-danger">{!! $user->email !!}</div>
                <div class="col-sm-3">Số điện thoại:</div>
                <div class="col-sm-9 text-danger">{!! $user->phone != null ? $user->phone : '&nbsp;' !!}</div>
                <div class="col-sm-3">Địa chỉ:</div>
                <div class="col-sm-9 text-danger">{!! $user->address != null ? $user->address : '&nbsp;' !!}</div>
                <div class="col-sm-3">Ngày sinh:</div>
                <div class="col-sm-9 text-danger">{!! date_format(date_create($user->birthday), 'd/m/Y') !!}</div>
                <div class="col-sm-3">Ngày đăng ký:</div>
                <div class="col-sm-9 text-muted">{!! $user->created_at !!}</div>
                <div class="col-sm-3">Lần thay đổi gần nhất:</div>
                <div class="col-sm-9 text-muted">{!! $user->updated_at !!}</div>
                <div class="col-sm-9 col-sm-offset-3"><a class="btn btn-primary btn-sm mar-top-10"
                                                         href="{!! route('account.edit', ['id' => \Illuminate\Support\Facades\Cookie::get('user_id')]) !!}">Sửa
                        thông
                        tin</a></div>
            </div>
        </div>
    </div>
@endsection