@extends('layouts.master')
@section('title')
    Sửa thông tin
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="register-page">
        <div class="container-fluid">
            <p class="lead text-danger">Sửa thông tin</p>

            @include('supporter.alert')

            <div class="row">
                {!! Form::model($user, [
                        'method' => 'post',
                         'url' => route('account.update', ['id' => $user->id]),
                         'class' => 'form-horizontal'
                    ]) !!}
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('username', 'Tên tài khoản', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::text('username', null, ['class' => 'form-control', 'required' => true, 'disabled' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::email('email', null, ['class' => 'form-control', 'required' => true, 'disabled' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => '3']) !!}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Tên người dùng', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Số điện thoại', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::text('phone', null, ['class' => 'form-control']) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('birthday', 'Ngày sinh', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::date('birthday',  null, ['class' => 'form-control']) !!}</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-3 ">{!! Form::submit('Cập nhật', ['class' => 'form-control btn-primary']) !!}</div>
                        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('account.profile', ['id' => $user->id]) !!}">Hủy</a></div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection