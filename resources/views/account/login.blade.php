@extends('layouts.master')
@section('title')
    Đăng nhập
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="login-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(count($errors) > 0 || session()->has('message_type'))
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-danger">{!! $error !!}</li>
                                    @endforeach
                                    @if(session()->has('message_type'))
                                        <li class="text-{!! session()->get('message_type') !!}">{!! session()->get('message_content') !!}</li>
                                    @endif
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    {!! Form::open([
                    'method' => 'post',
                    'route' => 'account.loginPost',
                    'class' => 'form-horizontal'
                    ]) !!}
                    <div class="form-group">
                        {!! Form::label('username', 'Tên tài khoản', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::text('username', '', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    {{--<div class="form-group">--}}
                    {{--<div class="col-sm-1 col-sm-offset-4 ">--}}
                    {{--{!! Form::checkbox('remember', '1', true) !!}--}}
                    {{--</div>--}}
                    {{--Ghi nhớ--}}
                    {{--</div>--}}
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4 ">
                            Nếu chưa có tài khoản? <a href="{!! route('account.register') !!}">Tạo mới</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4 ">{!! Form::submit('Đăng nhập', ['class' => 'form-control btn-primary']) !!}</div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection