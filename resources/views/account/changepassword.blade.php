@extends('layouts.master')
@section('title')
    Đổi mật khẩu
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="changepassword">
        <div class="container-fluid">
            <p class="lead">Đổi mật khẩu</p>

            @include('supporter.alert')

            <div class="row">
                <div class="col-sm-5">
                    {!! Form::open([
                    'method' => 'post',
                    'url' => route('account.updatePassword', ['id' => $user_id]),
                    'class' => 'form-horizontal'
                    ]) !!}
                    <div class="form-group">
                        {!! Form::label('old_password', 'Mật khẩu cũ', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('old_password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('new_password', 'Mật khẩu mới', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('new_password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('re_password', 'Nhập lại mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('re_password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4 ">{!! Form::submit('Cập nhật', ['class' => 'form-control btn-primary']) !!}</div>
                        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('account.profile', ['id' => $user_id]) !!}">Hủy</a></div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection