@extends('layouts.master')
@section('title')
    Đăng ký thành viên
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="register-page">
        <div class="container-fluid">
            <p class="lead text-danger">Đăng ký thành viên</p>

            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            @if(count($errors) > 0 || session()->has('message_type'))
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-danger">{!! $error !!}</li>
                                    @endforeach
                                    @if(session()->has('message_type'))
                                        <li class="text-{!! session()->get('message_type') !!}">{!! session()->get('message_content') !!}</li>
                                    @endif
                                </ul>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                {!! Form::open([
                        'method' => 'post',
                         'route' => 'account.registerPost',
                         'class' => 'form-horizontal'
                    ]) !!}
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('username', 'Tên đăng nhập', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::text('username', '', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('re_password', 'Nhập lại mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::password('re_password', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::email('email', '', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::textarea('address', '', ['class' => 'form-control', 'rows' => '3']) !!}</div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Tên người dùng', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::text('name', '', ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone_number', 'Số điện thoại', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::text('phone_number', '', ['class' => 'form-control']) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('birthday', 'Ngày sinh', ['class' => 'control-label col-sm-3 small']) !!}
                        <div class="col-sm-8">{!! Form::date('birthday',  'dd/mm/yyyy', ['class' => 'form-control']) !!}</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3 ">
                            Nếu đã có tài khoản? <a href="{!! route('account.login') !!}">Đăng nhập</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-3 ">{!! Form::submit('Tạo tài khoản', ['class' => 'form-control btn-primary']) !!}</div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection