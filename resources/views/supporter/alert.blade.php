<div class="row">
    <div class="col-sm-12">
        @if(count($errors) > 0 || session()->has('message_type'))
            @foreach($errors->all() as $error)
                <div class="text-danger">{!! $error !!}</div>
            @endforeach
            @if(session()->has('message_type'))
                <div class="alert alert-{!! session()->get('message_type') !!}">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    {!! session()->get('message_content') !!}</div>
            @endif
        @endif
    </div>
</div>