<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery('.btn-delete').on('click', function (event) {
            var chosen = confirm('Bạn muốn xóa {!! $name !!} có id là ' + jQuery(this).data('id') + ' ?');
            if (!chosen) {
                event.preventDefault();
            }
        });
    });
</script>