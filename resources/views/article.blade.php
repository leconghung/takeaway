@extends('layouts.master')
@section('title')
    {!! $article->title !!}
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/article.css') }}" type="text/css">
@endsection
@section('content')
    <div id="article">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6 pull-left">
                    <div class="pull-left">
                        <p class="lead text-danger">{!! $article->title !!}</p>
                    </div>
                    <div class="pull-right">
                        <div class="rating mar-top-5">
                            @for($i = 1; $i <= $article->getRatingNumber(); $i++ )
                                <img src="{!! asset('images/star-on.png') !!}" alt="1" title="good">
                            @endfor
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-left">
                        <p class="text-muted"><span>Ngày đăng:</span> {!! $article->created_at !!}</p>
                    </div>
                    <div class="pull-right">
                        <p class="text-info pad-right-10 badge"><span>Đã xem:</span> {!! $article->viewed !!}</p>
                    </div>
                    <div class="clearfix"></div>
                    <div class="vote">
                        <div class="row">
                            @if($article->canRate())
                                {!! Form::open([
                                    'method' => 'post',
                                    'url' => route('article.vote', ['id' => $article->id])
                                ]) !!}
                                <div class="col-sm-6"><span class="glyphicon glyphicon-thumbs-up"></span> <label for="">Đánh
                                        giá sản phẩm này:</label></div>
                                <div class="col-sm-1 col-sm-offset-3">{!! Form::select('point', [1=>1,2=>2,3=>3,4=>4,5=>5], 5, ['class' => '']) !!}</div>
                                <div class="col-sm-2">{!! Form::submit('Đánh giá', ['class' => 'btn btn-warning btn-xs']) !!}</div>
                                {!! Form::close() !!}
                            @else
                                @if(\Illuminate\Support\Facades\Cookie::has('login_flag'))
                                    <div class="col-sm-12 text-danger">Bạn đã đánh giá cho sản phẩm này
                                        ({!! $article->getRateOfCurrentUser() !!} <img
                                                src="{!! asset('images/star-on.png') !!}" alt="1" title="good">)
                                    </div>
                                @else
                                    <div class="col-sm-12"><a class="text-danger" href="{!! route('account.login') !!}">Đăng
                                            nhập để đánh giá cho sản phẩm!</a></div>
                                @endif
                            @endif
                        </div>
                    </div>

                    <p class="content">{!! $article->content !!}</p>

                    <div class="price text-left">
                        <label>Giá bán:</label>
                        <span class="new-price">{!! number_format($article->new_price, 0, '', '.') !!}đ</span>
                        @if($article->old_price != null)
                            <span class="old-price">{!! number_format($article->old_price, 0, '', '.') !!}đ</span>
                        @endif
                    </div>
                    <div class="cart mar-top-10">
                        <div class="row">
                            <div class="form-group-sm">
                                {!! Form::open([
                                    'method' => 'post',
                                    'url' => route('cart.add')
                                ]) !!}
                                <div class="col-sm-3">{!! Form::number('qty', 1, ['min' => 1, 'max' => 10, 'class' => 'form-control']) !!}</div>
                                @if($article->qty > 0)
                                    <div class="col-sm-4">{!! Form::submit('Mua ngay', ['class' => 'form-control btn-primary']) !!}</div>
                                @else
                                    <div class="col-sm-4">
                                        <button class="btn btn-warning btn-sm" title="Hết hàng">Hết hàng</button>
                                    </div>
                                @endif
                                {!! Form::hidden('pid', $article->id) !!}
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 pull-right">
                    <div class="image">
                        <img src="{!! asset($article->image) !!}" alt="{!! $article->title !!}"
                             title="{!! $article->title !!}">
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection