@extends('layouts.master')
@section('title')
    Xác nhận mua hàng
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/account.css') }}" type="text/css">
@endsection
@section('content')
    <div id="cartpage">
        <div class="container-fluid">
            <div class="row">
                <p class="lead text-danger">Bước cuối cùng! mời bạn vui lòng điền đầy đủ thông tin phía dưới</p>
                {!! Form::model($user, [
                    'method' => 'post',
                    'route' => 'cart.place',
                    'class' => 'form-horizontal'
                ]) !!}
                {!! Form::hidden('id', null) !!}
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('name', 'Họ tên:', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::email('email', null, ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('phone', 'Số điện thoại', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::text('phone', null, ['class' => 'form-control', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label col-sm-4 small']) !!}
                        <div class="col-sm-8">{!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-4">
                            {!! Form::submit('Mua hàng', ['class' => 'btn btn-danger']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    Xem hướng dẫn mua hàng <a target="_blank" href="{!! route('menu.show', ['id' => 4]) !!}">tại đây</a>
                    <br><br>
                    <ul>
                        <li>Thời gian chuyển hàng: <span class="text-info">Ngay sau khi được xác nhận của hỗ trợ viên. (Sau khi bạn đặt hàng,
                        chúng tôi sẽ gọi điện cho bạn để xác nhận lại một lần nữa!)</span>
                        </li>
                        <li>Chi phí chuyển hàng: <span class="text-info">Miễn phí đối với các vùng nội thành Hà nội,
                                với các vùng ngoại thành +20.000 tiền vận chuyển</span>
                        </li>
                        <li>Hình thức thanh toán: <span class="text-info">Trả tiền sau khi nhận được hàng</span></li>
                    </ul>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection