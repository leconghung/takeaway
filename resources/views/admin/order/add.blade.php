@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Tạo đơn hàng mới</p>

            <div class="row">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'admin.order.store',
                    'class' => 'form-horizontal',
                ]) !!}
                @include('admin.order._form', ['actionName' => 'Tạo đơn hàng'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection