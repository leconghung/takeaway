<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('article_id', 'Id sản phẩm', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::text('article_id', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('name', 'Tên khách hàng', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::email('email', null, ['class' => 'form-control']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('phone', 'Số điện thoại', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::text('phone', null, ['class' => 'form-control']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('qty', 'Số lượng', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-3">{!! Form::number('qty', null, ['class' => 'form-control', 'required' => true, 'min' => 1]) !!}</div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => '5']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::select('status', ['waiting' => 'Đang chờ', 'success' => 'Hoàn tất'], null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-3 ">{!! Form::submit($actionName, ['class' => 'form-control btn-primary']) !!}</div>
        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('admin.order.index') !!}">Hủy</a></div>
    </div>
</div>