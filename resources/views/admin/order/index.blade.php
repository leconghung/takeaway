@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Quản lý giao dịch</p></div>
        <div class="pull-right"><a href="{!! route('admin.order.add') !!}" class="btn btn-success btn-sm">Thêm mới</a></div>
        <div class="clearfix"></div>

        @include('supporter.alert')

        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Product</td>
                <td>User Id</td>
                <td>User Name</td>
                <td>Order Name</td>
                <td>Email</td>
                <td>Qty</td>
                <td>Phone</td>
                <td>Address</td>
                <td>Status</td>
                <td>Created At</td>
                <td>Updated At</td>
                <td>Confirm Order</td>
                <td>Cancel Order</td>
            </tr>
            @foreach($transactions as $transaction)
                <tr>
                    <td>{!! $transaction->id !!}</td>
                    <td>
                        <a href="{!! route('article.show', ['id' => $transaction->id]) !!}">{!! $transaction->getArticleName() !!}</a>
                    </td>
                    <td>{!! $transaction->user_id !!}</td>
                    <td>{!! $transaction->getUserName() !!}</td>
                    <td>{!! $transaction->name!!}</td>
                    <td>{!! $transaction->email !!}</td>
                    <td>{!! $transaction->qty !!}</td>
                    <td>{!! $transaction->phone !!}</td>
                    <td>{!! $transaction->address !!}</td>
                    @if($transaction->status == 'success')
                        <td><span class="label label-success">{!! $transaction->status !!}</span></td>
                    @elseif($transaction->status == 'waiting')
                        <td><span class="label label-warning">{!! $transaction->status !!}</span></td>
                    @else
                        <td><span class="label label-danger">{!! $transaction->status !!}</span></td>
                    @endif
                    <td>{!! date_format($transaction->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($transaction->updated_at, 'd/m/Y') !!}</td>
                    <td>
                        @if($transaction->status == 'waiting')
                            <a href="{!! route('admin.order.changeStatus', ['id' => $transaction->id, 'status' => 'success']) !!}"
                               title="Xác nhận đã chuyển hàng" class="text-primary btn-order-success"
                               data-id="{!! $transaction->id !!}"><span
                                        class="fa fa-check-square-o fa-fw "></span> Xác nhận</a>
                        @endif
                    </td>
                    <td>
                        @if($transaction->status != 'cancelled')
                            <a href="{!! route('admin.order.changeStatus', ['id' => $transaction->id, 'status' => 'cancelled']) !!}"
                               title="Hủy đơn hàng" class="text-danger btn-order-cancel" data-id="{!! $transaction->id !!}"><span
                                        class="fa fa-times-circle-o fa-fw"></span> Hủy</a>
                        @endif
                    </td>
                </tr>

            @endforeach
        </table>
        {!! $transactions->links() !!}
    </div>
@endsection
@section('body.js')
    @include('supporter.confirm-cancel')
    @include('supporter.confirm-success')
@endsection