<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập vào trang quản trị</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/supporter.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" type="text/css">
    <script src="{!! asset('js/jquery-2.2.4.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('js/bootstrap.min.js') !!}"></script>
</head>
<body>
<div class="container-fluid">
    <div id="admin-login">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="row">
                    <div class="col-sm-12">
                        @if(count($errors) > 0 || session()->has('message_type'))
                            @foreach($errors->all() as $error)
                                <div class="text-danger">{!! $error !!}</div>
                            @endforeach
                            @if(session()->has('message_type'))
                                <div class="alert alert-{!! session()->get('message_type') !!}">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    {!! session()->get('message_content') !!}</div>
                            @endif
                        @endif
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Đăng nhập vào trang quản trị</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open([
                            'method' => 'post',
                            'class' => 'form-horizontal',
                            'route' => 'admin.loginPost'
                        ]) !!}
                        {{ csrf_field() }}
                        <div class="form-group">
                            {!! Form::label('username', 'Tên tài khoản', ['class' => 'control-label col-sm-4 small']) !!}
                            <div class="col-sm-8">{!! Form::text('username', '', ['class' => 'form-control', 'required' => true]) !!}</div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
                            <div class="col-sm-8">{!! Form::password('password', ['class' => 'form-control', 'required' => true]) !!}</div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-4 ">{!! Form::submit('Đăng nhập', ['class' => 'form-control btn-primary']) !!}</div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>