<hr>
<p class="lead">Tổng sản phẩm đã bán: <span class="text-danger">{!! $totalQty !!} sản phẩm</span></p>
<p class="lead">Tổng doanh thu: <span class="text-danger">{!! number_format($totalPrice, 0, '', '.') !!} đ</span></p>

<hr>
<p class="lead">Thống kê theo ngày, tuần, tháng, năm</p>
<table class="table table-bordered table-hover">
    <tr class="active">
        <td width="150px">Chỉ số</td>
        <td>Trong ngày ({!! date('d/m/Y') !!})</td>
        <td>Trong tuần ({!! date('d/m/Y', strtotime('last monday')) !!}
            - {!! date('d/m/Y', strtotime('next sunday')) !!})
        </td>
        <td>Trong tháng ({!! date('d/m/Y', strtotime('first day of this month')) !!}
            - {!! date('d/m/Y', strtotime('last day of this month')) !!})
        </td>
        <td>Trong năm ({!! date('01/01/Y') !!} - {!! date('31/12/Y') !!})</td>
    </tr>
    <tr>
        <td>Doanh thu</td>
        <td>{!! number_format($revenueByDay, 0, '', '.') !!} đ</td>
        <td>{!! number_format($revenueByWeek, 0, '', '.') !!} đ</td>
        <td>{!! number_format($revenueByMonth , 0, '', '.') !!} đ</td>
        <td>{!! number_format($revenueByYear , 0, '', '.') !!} đ</td>
    </tr>
    <tr>
        <td>Số hóa đơn mới</td>
        <td>{!! $invoiceDay !!}</td>
        <td>{!! $invoiceWeek !!}</td>
        <td>{!! $invoiceMonth !!}</td>
        <td>{!! $invoiceYear !!}</td>
    </tr>
    <tr>
        <td>Lượng người dùng mới</td>
        <td>{!! $accountDay !!}</td>
        <td>{!! $accountWeek !!}</td>
        <td>{!! $accountMonth !!}</td>
        <td>{!! $accountYear !!}</td>
    </tr>
    <tr>
        <td>Sản phẩm mới</td>
        <td>{!! $productDay !!}</td>
        <td>{!! $productWeek !!}</td>
        <td>{!! $productMonth !!}</td>
        <td>{!! $productYear !!}</td>
    </tr>
    <tr>
        <td>Lượng khách đã ghé thăm</td>
        <td>{!! $onlineDay !!}</td>
        <td>{!! $onlineWeek !!}</td>
        <td>{!! $onlineMonth !!}</td>
        <td>{!! $onlineYear !!}</td>
    </tr>
</table>

<hr>
<p class="lead">Top 10 sản phẩm bán chạy nhất: </p>
<table class="table table-bordered table-hover">
    <tr class="active">
        <td>STT</td>
        <td>Tên sản phẩm</td>
        <td>Số lượng đã bán</td>
        <td>Doanh thu</td>
    </tr>

    <?php $count = 0; ?>
    @foreach($transactions as $transaction)
        <tr>
            <td>{!! $count++ !!}</td>
            <td>
                <a href="{!! route('article.show', ['id' => $transaction->article_id]) !!}">{!! $transaction->getArticleName() !!}</a>
            </td>
            <td class="text-danger">{!! $transaction->total_qty !!}</td>
            <td class="text-danger">{!! number_format($transaction->total_price, 0, '', '.') !!} đ</td>
        </tr>
    @endforeach
</table>