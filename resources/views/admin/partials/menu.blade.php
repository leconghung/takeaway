<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li><a href="{!! route('admin.dashboard') !!}"><i class="fa fa-dashboard fa-fw"></i> Tổng quan - Thống
                    kê</a></li>

            <li>
                <a style="cursor: pointer"><i class="fa fa-user fa-fw"></i> Tài khoản<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{!! route('admin.user.index') !!}"><span class="fa fa-user fa-fw"></span> Người
                            dùng</a></li>
                    <li><a href="{!! route('admin.member.index') !!}"><span class="fa fa-user-md fa-fw"></span> Quản trị
                            viên</a></li>
                </ul>
                <!-- /.nav-second-level -->
            </li>

            <li>
                <a style="cursor: pointer"><i class="fa  fa-pencil-square-o fa-fw"></i> Sản phẩm<span class="fa arrow"></a>
                <ul class="nav nav-second-level">
                    <li><a href="{!! route('admin.article.article.index') !!}"><i class="fa fa-pencil fa-fw"></i> Danh
                            mục bài viết</a></li>
                    <li><a href="{!! route('admin.article.category.index') !!}"><i class="fa fa-list-ul fa-fw"></i>
                            Chuyên mục</a></li>
                    <li><a href="{!! route('admin.article.menu.index') !!}"><i class="fa fa-sitemap fa-fw"></i> Menu</a>
                    </li>
                </ul>
            </li>

            <li><a href="{!! route('admin.order.index') !!}"><i class="fa fa-shopping-cart fa-fw"></i> Giao dịch</a></li>
            <li><a href="{!! route('admin.rating.index') !!}"><i class="fa fa-star-half-empty fa-fw"></i> Đánh giá</a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->