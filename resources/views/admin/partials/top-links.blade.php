<ul class="nav navbar-top-links navbar-right">
    <li class="navbar-text">
        Xin chào, {!! \Illuminate\Support\Facades\Cookie::get('admin_name') !!}
    </li>
    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
        </a>
        <ul class="dropdown-menu dropdown-user">
            <li><a href="{!! route('admin.member.index') !!}"><i class="fa fa-user fa-fw"></i> Thông tin tài khoản</a>
            </li>
            <li class="divider"></li>
            <li><a href="{!! route('admin.logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Đăng xuất</a>
            </li>
        </ul>
        <!-- /.dropdown-user -->
    </li>
    <!-- /.dropdown -->
</ul>
<!-- /.navbar-top-links -->