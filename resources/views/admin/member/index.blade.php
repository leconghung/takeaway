@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Danh sách quản trị viên </p></div>
        {{--<div class="pull-right">@if(\Illuminate\Support\Facades\Cookie::get('admin_role') == 1) <a href=""--}}
                                                                                                   {{--class="btn btn-success btn-sm">Thêm--}}
                {{--mới</a>@endif--}}
        {{--</div>--}}
        <div class="clearfix"></div>
        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Username</td>
                <td>Name</td>
                <td>Email</td>
                <td>Role</td>
                <td>Status</td>
                <td>Created At</td>
                <td>Updated At</td>
                <td colspan="2">Action</td>
            </tr>
            @foreach($members as $member)
                <tr>
                    <td>{!! $member->id !!}</td>
                    <td>{!! $member->username !!}</td>
                    <td>{!! $member->name !!}</td>
                    <td>{!! $member->email !!}</td>
                    <td>{!! $member->getAdminRoleName() !!}</td>
                    <td>{!! ($member->status == 1) ? "Active" : "Inactive" !!}</td>
                    <td>{!! date_format($member->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($member->updated_at, 'd/m/Y') !!}</td>
                    <td>@if(\Illuminate\Support\Facades\Cookie::get('admin_role') == 1 && $member->role != 1) <a href=""
                                                                                                                 title="Sửa"><span
                                    class="fa fa-pencil fa-fw"></span></a> @endif</td>
                    <td>@if(\Illuminate\Support\Facades\Cookie::get('admin_role') == 1 && $member->role != 1) <a
                                href="{!! route('admin.member.destroy', ['id' => $member->id]) !!}" class="btn-delete"
                                data-id="{!! $member->id !!}"
                                title="Xóa"><span
                                    class="fa fa-trash-o fa-fw"></span></a> @endif</td>
                </tr>
            @endforeach
        </table>
        {!! $members->links() !!}
    </div>
@endsection
@section('body.js')
    @include('supporter.confirm-delete', ['name' => 'quản trị viên'])
@endsection