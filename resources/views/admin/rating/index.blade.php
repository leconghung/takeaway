@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Đánh giá sản phẩm bởi người dùng</p></div>
        <div class="clearfix"></div>

        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Product</td>
                <td>User</td>
                <td>Rating</td>
                <td>Created At</td>
                <td>Updated At</td>
            </tr>
            @foreach($ratings as $rating)
                <tr>
                    <td>{!! $rating->id !!}</td>
                    <td>
                        <a href="{!! route('article.show', ['id' => $rating->id]) !!}">{!! $rating->getArticleName() !!}</a>
                    </td>
                    <td>{!! $rating->getUserName() !!}</td>
                    <td>{!! $rating->rating !!}</td>
                    <td>{!! date_format($rating->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($rating->updated_at, 'd/m/Y') !!}</td>
                </tr>

            @endforeach
        </table>
        {!! $ratings->links() !!}
    </div>
@endsection