@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Sửa chuyên mục</p>

            <div class="row">
                {!! Form::model($category, [
                    'method' => 'post',
                    'route' => 'admin.article.category.update',
                    'class' => 'form-horizontal',
                ]) !!}
                {!! Form::hidden('id', $category->id) !!}
                @include('admin.article._form_category', ['actionName' => 'Cập nhật'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection