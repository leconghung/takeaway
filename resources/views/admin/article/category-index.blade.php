@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Quản lý chuyên mục</p></div>
        <div class="pull-right"><a href="{!! route('admin.article.category.add') !!}" class="btn btn-success btn-sm">Thêm mới</a></div>
        <div class="clearfix"></div>

        @include('supporter.alert')

        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Name</td>
                <td>Level</td>
                <td>Parent Id</td>
                <td>Has Children</td>
                <td>Status</td>
                <td>Created at</td>
                <td>Updated at</td>
                <td colspan="2">Action</td>
            </tr>
            @foreach($categories as $category)
                <tr>
                    <td>{!! $category->id !!}</td>
                    <td>{!! $category->name !!}</td>
                    <td>{!! $category->level !!}</td>
                    <td>{!! $category->parent_id !!}</td>
                    <td>{!! ($category->has_children) ? "Có" : "Không" !!}</td>
                    <td>{!! ($category->status == 1) ? "Hiển thị" : "Không hiển thị" !!}</td>
                    <td>{!! date_format($category->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($category->updated_at, 'd/m/Y') !!}</td>
                    <td><a href="{!! route('admin.article.category.edit', ['id' => $category->id]) !!}" title="Sửa"><span class="fa fa-pencil fa-fw"></span></a></td>
                    <td><a href="{!! route('admin.article.category.destroy', ['id' => $category->id]) !!}" title="Xóa"
                           class="btn-delete" data-id="{!! $category->id !!}"><span class="fa fa-trash-o fa-fw"></span></a>
                    </td>
                </tr>
            @endforeach
        </table>
        {!! $categories->links() !!}
    </div>
@endsection
@section('body.js')
    @include('supporter.confirm-delete', ['name' => 'chuyên mục'])
@endsection