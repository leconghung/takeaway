@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Tạo sản phẩm mới</p>

            <div class="row">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'admin.article.article.store',
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ]) !!}
                @include('admin.article._form_article', ['actionName' => 'Tạo tài khoản'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection