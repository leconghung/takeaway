@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Danh mục sản phẩm</p></div>
        <div class="pull-right"><a href="{!! route('admin.article.article.add') !!}" class="btn btn-success btn-sm">Thêm
                mới</a></div>
        <div class="clearfix"></div>

        @include('supporter.alert')

        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Title</td>
                <td>Category</td>
                <td>Viewed</td>
                <td>Qty</td>
                <td>Old Price</td>
                <td>New Price</td>
                <td>Featured</td>
                <td>Image Path</td>
                <td>Author</td>
                <td>Status</td>
                <td colspan="2">Action</td>
            </tr>
            @foreach($articles as $article)
                <tr>
                    <td>{!! $article->id !!}</td>
                    <td><a target="_blank" title="Click to show this product in front end!"
                           href="{!! route('article.show', ['id' => $article->id]) !!}">{!! $article->title !!}</a></td>
                    <td>{!! $article->getCategoryName() !!}</td>
                    <td>{!! $article->viewed !!}</td>
                    <td>{!! $article->qty !!}</td>
                    <td>{!! number_format($article->old_price, 0, '', '.') !!}</td>
                    <td>{!! number_format($article->new_price, 0, '', '.') !!}</td>
                    <td>{!! ($article->special_flag == 1) ? "Có" : "Không" !!}</td>
                    <td><img src="{!! asset($article->image) !!}" alt="" style="max-width: 50px; height: auto"></td>
                    <td>{!! $article->getAuthorName() !!}</td>
                    <td>{!! $article->status == 1 ? 'Hiển thị' : 'Không hiển thị' !!}</td>
                    <td><a href="{!! route('admin.article.article.edit', ['id' => $article->id]) !!}" title="Sửa"><span
                                    class="fa fa-pencil fa-fw"></span></a></td>
                    <td><a href="{!! route('admin.article.article.destroy', ['id' => $article->id]) !!}"
                           title="Xóa" class="btn-delete" data-id="{!! $article->id !!}"><span
                                    class="fa fa-trash-o fa-fw"></span></a></td>
                </tr>

            @endforeach
        </table>
        {!! $articles->links() !!}
    </div>
@endsection
@section('body.js')
    @include('supporter.confirm-delete', ['name' => 'sản phẩm'])
@endsection
