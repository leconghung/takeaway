@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Sửa menu</p>

            <div class="row">
                {!! Form::model($menu, [
                    'method' => 'post',
                    'route' => 'admin.article.menu.update',
                    'class' => 'form-horizontal',
                ]) !!}
                {!! Form::hidden('id', $menu->id) !!}
                @include('admin.article._form_menu', ['actionName' => 'Cập nhật'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection