@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Tạo menu mới</p>

            <div class="row">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'admin.article.menu.store',
                    'class' => 'form-horizontal',
                ]) !!}
                @include('admin.article._form_menu', ['actionName' => 'Tạo menu'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection