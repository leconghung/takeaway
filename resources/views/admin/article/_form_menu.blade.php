<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('name', 'Tên', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('identifier', 'Định danh', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('identifier', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('position', 'Vị trí', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('position', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('content', 'Nội dung', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8 ">{!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '10' ]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::select('status', ['1' => 'Hiển thị', '0' => 'Không hiển thị'], null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-4 ">{!! Form::submit($actionName, ['class' => 'form-control btn-primary']) !!}</div>
        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('admin.article.menu.index') !!}">Hủy</a></div>
    </div>
</div>