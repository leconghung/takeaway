<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('name', 'Tên', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('level', 'Cấp của chuyên mục', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-3">{!! Form::number('level', null, ['class' => 'form-control', 'required' => true, 'min' => 1, 'max' => 2, 'placeholder' => '1 or 2']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('parent_id', 'Chuyên mục cha', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::select('parent_id', $categoryList, null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('has_children', 'Có chuyên mục con?', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-3 mar-top-5">{!! Form::checkbox('has_children', null) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::select('status', ['1' => 'Hiển thị', '0' => 'Không hiển thị'], null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-4 ">{!! Form::submit($actionName, ['class' => 'form-control btn-primary']) !!}</div>
        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('admin.article.category.index') !!}">Hủy</a></div>
    </div>
</div>