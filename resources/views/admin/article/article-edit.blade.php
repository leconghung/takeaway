@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Thay đổi thông tin sản phẩm</p>

            <div class="row">
                {!! Form::model($article, [
                    'method' => 'post',
                    'route' => 'admin.article.article.update',
                    'class' => 'form-horizontal',
                    'enctype' => 'multipart/form-data'
                ]) !!}
                {!! Form::hidden('id', $article->id) !!}
                @include('admin.article._form_article', ['actionName' => 'Cập nhật'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection