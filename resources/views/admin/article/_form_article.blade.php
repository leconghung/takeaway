<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('title', 'Tiêu đề', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::text('title', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('qty', 'Số lượng', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-3">{!! Form::number('qty', null, ['class' => 'form-control', 'required' => true, 'min' => 0]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('old_price', 'Giá cũ', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-6">{!! Form::text('old_price', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'đ']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('new_price', 'Giá mới', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-6">{!! Form::text('new_price', null, ['class' => 'form-control', 'required' => true, 'placeholder' => 'đ']) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('content', 'Nội dung', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-9">{!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => '10', 'required' => true]) !!}</div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('special_flag', 'Sản phẩm nổi bật', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-3 mar-top-5">{!! Form::checkbox('special_flag', null) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('image', 'Hình ảnh', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::file('image') !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('category_id', 'Chuyên mục', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::select('category_id', $categoryList, null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('author_id', 'Tác giả', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::select('author_id', $authorList, null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('status', 'Trạng thái', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::select('status', ['1' => 'Hiển thị', '0' => 'Không hiển thị'], null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-3 ">{!! Form::submit($actionName, ['class' => 'form-control btn-primary']) !!}</div>
        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('admin.article.article.index') !!}">Hủy</a></div>
    </div>
</div>