@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead">Tạo chuyên mục mới</p>

            <div class="row">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'admin.article.category.store',
                    'class' => 'form-horizontal',
                ]) !!}
                @include('admin.article._form_category', ['actionName' => 'Tạo chuyên mục'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection