@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Quản lý menu</p></div>
        <div class="pull-right"><a href="{!! route('admin.article.menu.add') !!}" class="btn btn-success btn-sm">Thêm mới</a></div>
        <div class="clearfix"></div>

        @include('supporter.alert')

        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Name</td>
                <td>Identifier</td>
                <td>Position</td>
                <td>Content</td>
                <td>Status</td>
                <td>Created at</td>
                <td>Updated at</td>
                <td colspan="2">Action</td>
            </tr>
            @foreach($menus as $menu)
                <tr>
                    <td>{!! $menu->id !!}</td>
                    <td>{!! $menu->name !!}</td>
                    <td>{!! $menu->identifier !!}</td>
                    <td>{!! $menu->position !!}</td>
                    <td>{!! substr($menu->content, 0, 50) !!}</td>
                    <td>{!! ($menu->status == 1) ? "Hiển thị" : "Không hiển thị" !!}</td>
                    <td>{!! date_format($menu->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($menu->updated_at, 'd/m/Y') !!}</td>
                    <td><a href="{!! route('admin.article.menu.edit', ['id' => $menu->id]) !!}" title="Sửa"><span class="fa fa-pencil fa-fw"></span></a></td>
                    <td><a class="btn-delete" data-id="{!! $menu->id !!}"
                           href="{!! route('admin.article.menu.destroy', ['id' => $menu->id]) !!}" title="Xóa"><span
                                    class="fa fa-trash-o fa-fw"></span></a></td>
                </tr>
            @endforeach
        </table>
        {!! $menus->links() !!}
    </div>
@endsection

@section('body.js')
    @include('supporter.confirm-delete', ['name' => 'menu'])
@endsection