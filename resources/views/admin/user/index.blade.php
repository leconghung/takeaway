@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="pull-left"><p class="lead">Danh sách người dùng</p></div>
        <div class="pull-right"><a href="{!! route('admin.user.add') !!}" class="btn btn-success btn-sm">Thêm mới</a>
        </div>
        <div class="clearfix"></div>
        @include('supporter.alert')
        <table class="table table-bordered table-hover">
            <tr class="active">
                <td>ID</td>
                <td>Username</td>
                <td>Name</td>
                <td>Email</td>
                <td>Phone</td>
                <td>Birthday</td>
                <td>Address</td>
                <td>Created at</td>
                <td>Updated at</td>
                <td colspan="2">Action</td>
            </tr>
            @foreach($users as $user)
                <tr>
                    <td>{!! $user->id !!}</td>
                    <td>{!! $user->username !!}</td>
                    <td>{!! $user->name !!}</td>
                    <td>{!! $user->email !!}</td>
                    <td>{!! $user->phone !!}</td>
                    <td>{!! date_format(date_create($user->birthday), 'd/m/Y') !!}</td>
                    <td>{!! $user->address !!}</td>
                    <td>{!! date_format($user->created_at, 'd/m/Y') !!}</td>
                    <td>{!! date_format($user->updated_at, 'd/m/Y') !!}</td>
                    <td><a href="{!! route('admin.user.edit', ['id' => $user->id]) !!}" title="Sửa"><span
                                    class="fa fa-pencil fa-fw"></span></a></td>
                    <td><a href="{!! route('admin.user.destroy', ['id' => $user->id]) !!}" class="btn-delete"
                           data-id="{!! $user->id !!}" title="Xóa"><span class="fa fa-trash-o fa-fw"></span></a></td>
                </tr>
            @endforeach
        </table>
        {!! $users->links() !!}
    </div>
@endsection
@section('body.js')
    @include('supporter.confirm-delete', ['name' => 'người dùng'])
@endsection