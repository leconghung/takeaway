@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead text-primary">Tạo tài khoản người dùng</p>
            <div class="row">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'admin.user.store',
                    'class' => 'form-horizontal'
                ]) !!}
                @include('admin.user._form', ['actionName' => 'Tạo tài khoản'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection