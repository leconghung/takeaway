@extends('admin.master')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <p class="lead text-primary">Sửa tài khoản</p>

            <div class="row">
                {!! Form::model($user,[
                    'method' => 'post',
                    'route' => 'admin.user.update',
                    'class' => 'form-horizontal'
                ]) !!}
                {!! Form::hidden('id', $user->id) !!}
                @include('admin.user._form', ['actionName' => 'Cập nhật tài khoản'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection