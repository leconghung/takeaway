<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('username', 'Tên tài khoản', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('username', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('password', 'Mật khẩu', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::text('password', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::email('email', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('address', 'Địa chỉ', ['class' => 'control-label col-sm-4 small']) !!}
        <div class="col-sm-8">{!! Form::textarea('address', null, ['class' => 'form-control', 'rows' => '3', 'required' => true]) !!}</div>
    </div>
</div>
<div class="col-sm-6">
    <div class="form-group">
        {!! Form::label('name', 'Tên người dùng', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('phone', 'Số điện thoại', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::text('phone', null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        {!! Form::label('birthday', 'Ngày sinh', ['class' => 'control-label col-sm-3 small']) !!}
        <div class="col-sm-8">{!! Form::date('birthday',  null, ['class' => 'form-control', 'required' => true]) !!}</div>
    </div>
    <div class="form-group">
        <div class="col-sm-4 col-sm-offset-3 ">{!! Form::submit($actionName, ['class' => 'form-control btn-primary']) !!}</div>
        <div class="col-sm-2"><a class="btn btn-danger" href="{!! route('admin.user.index') !!}">Hủy</a></div>
    </div>
</div>