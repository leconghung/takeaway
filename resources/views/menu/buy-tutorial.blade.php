@extends('layouts.master')
@section('title')
    Hướng dẫn mua hàng
@endsection
@section('head')
@endsection
@section('content')
    <div class="container-fluid mar-top-15">
        <div class="wrapper">
            <p class="lead">Hướng dẫn mua hàng</p>
            <ul>
                <li> - Quý khách gọi điện thoại trực tiếp đến cửa hàng Take away gần Quý khách nhất để đặt hàng.</li>
                <li> - Quý khách cũng có thể gọi đến số của nhân viên bán hàng bất kỳ của chúng tôi có trên website.</li>
                <li> - Quý khách có thể đặt hàng qua các trang mạng xã hội của chúng tôi như Facebook, Google+ (Lưu ý các
                    trang
                    này luôn có Logo và địa chỉ website của chúng tôi)
                </li>
                <li> - Chúng tôi chỉ chấp nhận các đơn đặt hàng được cung cấp đầy đủ các thông tin như tên người đặt hàng,
                    số
                    điện thoại (Take away có thể yêu cầu số điện thoại cố định trong một số trường hợp), địa chỉ.
                </li>
                <li> - Do đặc thù của đồ uống không thể đem đi quá xa, Take away sẽ cố gắng đưa hàng nhanh nhất tới tay Quý
                    khách hàng. Trong nhiều trường hợp,có xảy ra sơ suất, rất mong Quý khách thông cảm và thanh toán theo
                    thực tế.
                </li>
            </ul>
        </div>
    </div>
@endsection