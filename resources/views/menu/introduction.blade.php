@extends('layouts.master')
@section('title')
    Giới thiệu
@endsection
@section('head')
@endsection
@section('content')
    <div class="container-fluid mar-top-15">
        <div class="wrapper">
            <p class="lead">Giới thiệu</p>

            <p>
                Có mặt tại Việt Nam từ năm 2004 nhưng mãi đến nửa cuối năm 2012, cafe take away mới thực sự phát triển rầm
                rộ. Dần dần phong cách cafe này đã thay đổi khẩu vị và thu hút được sự yêu thích của rất nhiều bạn trẻ. Nếu
                trước đây thức uống yêu thích của các bạn trẻ là trà sữa, trà chanh hoặc cafe truyền thống thì giờ đây, cafe
                take away đã thật sự trở thành món uống được ưa thích hàng đầu bởi hương vị thơm ngon và màu sắc hấp dẫn.
                <br/><br/>
                Đúng với tên gọi, dạng cafe này mang đến cho thực khách những trải nghiệm thú vị, khác xa với kiểu thưởng
                thức cafe truyền thống. Trước đây muốn uống các loại cafe kiểu Tây này phải đến những thương hiệu nổi tiếng
                như Coffee Bean, Gloria Jeans, Angel in Us hay IIly cafe… với giá khá cao (trên 50 nghìn/ly). Bắt nhịp nhu
                cầu của giới trẻ, nhiều doanh nghiệp và tư nhân đã đầu tư để kinh doanh cafe take away . Các thức uống tiêu
                biểu của dạng cafe này như Cookies ice blended (bánh Oreo xay nhuyễn), Cappuccino, Latte, Mocha, Chocolate
                đá xay, các loại Yogurt với hình thức chung là bắt mắt, dễ thương. Mô hình cafe-bánh ngọt đã mang đến một
                phong cách ẩm thực mới, hiện đại mà vẫn đầy màu sắc và hương vị như chính cuộc sống của giới trẻ hiện nay.
                <br/><br/>
                Cafe-bánh ngọt take away trở thành trào lưu của các bạn trẻ lẫn dân văn phòng. Sự kết hợp thú vị giữa vị
                đắng của cafe, vị thanh nhẹ của các loại trà và thơm ngọt của bánh làm cho bữa ăn thêm ngon miệng hơn. Bánh
                kem và bánh ngọt với nhiều hương vị khác nhau là nhu cầu không thể thiếu của tất cả người dân hiện nay.
                Trong một bữa tiệc sinh nhật thì không thể thiếu bánh kem, và bánh ngọt bây giờ được nhiều làm đồ ăn nhẹ cho
                bữa sáng, vì vậy có thể nói đây là một sản phẩm tiềm năng. Cửa hàng sẽ những đáp ứng nhu cầu cao của khách
                hàng đặc biệt là giới trẻ, bắt nhịp cùng cuộc sống, mà còn góp phần vào sự phát triển kinh tế xã hội. Xã hội
                ngày càng phát triển, con người tham gia rất nhiều hoạt động, nhiều công việc để có thể đáp ứng được nhu cầu
                của bản thân song con người lại càng có ít thời gian để thư giãn và nghỉ ngơi, vì vậy một cửa hàng bánh ngọt
                sẽ đáp ứng được nhu cầu thư giãn hàng ngày trong những bộn bề của cuộc sống.
                <br/><br/>
            </p>
        </div>
    </div>
@endsection