@extends('layouts.master')
@section('title')
    Hướng dẫn thánh toán
@endsection
@section('head')
@endsection
@section('content')
    <div class="container-fluid mar-top-15">
        <div class="wrapper">
            <p class="lead">Hướng dẫn thánh toán</p>
            <ul>
                <li>
                    1. Giao nhận hàng:
                    <ul>
                        <li>- Take away miễn phí vận chuyển cho các đơn hàng mua từ 5 sản phẩm hoặc từ 250.000 đồng trở lên
                            trong bán
                            kính 5 km.
                        </li>
                        <li>- Trường hợp Quý khách hàng yêu cầu các địa chỉ giao hàng không cụ thể, chính xác, hoặc các địa
                            chỉ quá xa,
                            hoặc nếu Take away cảm thấy khu vực chuyển hàng có thể không an toàn, gây nguy hiểm cho người
                            giao hàng thì
                            Take away có thể từ chối giao hàng trực tiếp, mong Quý khách thông cảm.
                        </li>
                        <li> - Hàng được giao bởi Take away luôn đi kèm với 1 hóa đơn bán hàng. Trên hóa đơn luôn đi kèm và
                            ghi rõ: Logo
                            của Take away, Địa chỉ, số điện thoại và thông tin cá nhân của khách hàng. Thông tin này nếu
                            không trùng
                            khớp với các thông tin trên website thì khách hàng cần liên hệ ngay với bộ phận hỗ trợ khách
                            hàng của Take
                            away.
                        </li>
                        <li>- Quý khách cần kiểm tra kĩ số lượng, chất lượng và kí xác nhận vào phiếu nhận hàng và thanh
                            toán.
                        </li>
                    </ul>
                </li>
                <br>
                <li>
                    2. Thanh toán:
                    Take away hỗ trợ 3 hình thức thanh toán phổ biến như sau:
                    <ul>
                        <li>a. Tại cửa hàng: Khách hàng đến lấy hàng và thanh toán tại cửa hàng của Take away.</li>
                        <li>b. Thanh toán khi nhận hàng: Quý khách hàng thanh toán trực tiếp cho người giao hàng, theo hóa
                            đơn bán hàng,
                            ngay sau khi nhận hàng.
                        </li>
                        <li>c. Chuyển khoản ngân hàng:
                            Sau khi nhận được thông tin đặt hàng, chúng tôi sẽ tiến hành làm đồ và đóng hộp. Chúng tôi sẽ
                            thông báo ngay
                            cho khách hàng tổng tiền cần thanh toán.
                        </li>
                        <li></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
@endsection