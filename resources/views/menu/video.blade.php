@extends('layouts.master')
@section('title')
    Video
@endsection
@section('head')
@endsection
@section('content')
    <div class="container-fluid mar-top-15">
        <div class="wrapper text-center">
            <p class="lead text-danger">Cách làm cà phê đá xay - Cookie Cream cực ngon</p>
            <video width="854" height="480" controls>
                <source src="{!! asset('videos/Cookie_Cream.mp4') !!}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
            <hr>
            <p class="lead text-danger">Cách làm bánh Tiramisu đơn giản</p>
            <video width="854" height="480" controls>
                <source src="{!! asset('videos/Tiramisu.mp4') !!}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
            <hr>
            <p class="lead text-danger">Hướng dẫn làm Panna Cotta</p>
            <video width="854" height="480" controls>
                <source src="{!! asset('videos/Panna_Cotta.mp4') !!}" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </div>
@endsection