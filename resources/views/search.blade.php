@extends('layouts.master')
@section('title')
    Tìm kiếm
@endsection
@section('head')
    <link rel="stylesheet" href="{{ asset('css/article.css') }}" type="text/css">
@endsection
@section('content')
    <div id="product-list">
        <div class="container-fluid">
            <div class="filter">
                {!! Form::open([
                    'method' => 'post',
                    'route' => 'article.searchPost',
                ]) !!}
                {!! Form::hidden('advanced', 1) !!}
                <div class="row form-inline">
                    <div class="form-group">
                        {!! Form::label('searchKey', 'Từ khóa:', ['class' => 'control-label', 'style' => 'min-width: 70px;']) !!}
                        {!! Form::text('searchKey', $searchKey, ['class' => 'form-control', 'required' => true]) !!}
                    </div>
                    <div class="form-group mar-left-20">
                        {!! Form::label('category', 'Chuyên mục:', ['class' => 'control-label']) !!}
                        {!! Form::select('category', $categoryList, $categoryId, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="row form-inline mar-top-15">
                    <div class="form-group">
                        {!! Form::label('start_price', 'Giá:', ['class' => 'control-label', 'style' => 'min-width: 70px;']) !!}
                        <div class="input-group">
                            {!! Form::text('start_price', $start_price, ['class' => 'form-control']) !!}
                            <div class="input-group-addon">đ</div>
                        </div>
                    </div>
                    <div class="form-group">
                        <span class="glyphicon glyphicon-arrow-right"></span>
                        <div class="input-group">
                            {!! Form::text('end_price', $end_price, ['class' => 'form-control']) !!}
                            <div class="input-group-addon">đ</div>
                        </div>
                    </div>
                    {!! Form::submit('Lọc', ['class' => 'btn btn-primary mar-left-20']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <hr>
            @if(count($articles) == 0)
                <p class="lead text-info">Không tìm thấy kết quả nào!</p>
            @endif
            <div class="row">
                @foreach($articles as $article)
                    <div class="col-sm-3">
                        <div class="product-item">
                            <div class="title"><a
                                        href="{!! route('article.show', ['id' => $article->id]) !!}">{!! $article->title !!}</a>
                            </div>
                            <div class="image"><a href="{!! route('article.show', ['id' => $article->id]) !!}"><img
                                            src="{!! asset($article->image) !!}"
                                            alt="{!! $article->title !!}"
                                            title="{!! $article->title !!}"></a>
                            </div>
                            <div class="price">
                                <span class="new-price">{!! number_format($article->new_price, 0, '', '.') !!} đ</span>
                                @if($article->old_price != null)
                                    <span class="old-price">{!! number_format($article->old_price, 0, '', '.') !!}
                                        đ</span>
                                @endif
                            </div>
                            <div class="rating">
                                @for($i = 1; $i <= $article->getRatingNumber(); $i++ )
                                    <img src="{!! asset('images/star-on.png') !!}" alt="1" title="good">
                                @endfor
                            </div>
                            <div class="view-buy">
                                <div class="view pull-left">
                                    <label for="">Lượt xem: {!! $article->viewed !!}</label>
                                </div>
                                <div class="buy pull-right">
                                    {!! Form::open([
                                        'method' => 'post',
                                        'url' => route('cart.add')
                                    ]) !!}
                                    @if($article->qty > 0)
                                        {!! Form::submit('Mua ngay', ['class' => 'form-control btn-primary']) !!}
                                    @else
                                        <a href="{!! route('article.show', ['id' => $article->id]) !!}"
                                           class="btn btn-warning btn-sm">Xem</a>
                                    @endif
                                    {!! Form::hidden('pid', $article->id) !!}
                                    {!! Form::hidden('qty', 1) !!}
                                    {!! Form::close() !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {!! $articles->links() !!}
        </div>
    </div>
@endsection