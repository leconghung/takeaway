/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : 127.0.0.1:3306
Source Database       : takeaway

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-06-04 00:31:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_user
-- ----------------------------
DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `role` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_user
-- ----------------------------
INSERT INTO `admin_user` VALUES ('1', 'admin', 'admin123', 'Nga Le', 'lengatkt56@gmail.com', '1', '1', '2016-05-25 21:00:00', '2016-05-25 21:00:00');

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `special_flag` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `old_price` int(11) NOT NULL,
  `new_price` int(11) NOT NULL,
  `viewed` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  CONSTRAINT `article_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '6', '99', 'Black Forest', 'Black forest là tên gọi riêng của một loại bánh, một món dessert có nguồn gốc từ Đức. Là một dạng layer cake, bánh có thể có 2, 3 hoặc 4 lớp bánh, ở giữa là lớp kem whipping và quả cherry (anh đào). Linh đã ấp ủ món bánh này từ lâu nhưng đến nay mới làm được, lý do không phải vì bánh khó làm mà là khó mua nguyên liệu và nguyên liệu cũng đắt (nhất là khi tự làm ở nhà!) Nhưng Linh đã đặc biệt làm chiếc bánh này cho bữa party nhỏ tại nhà mình để mời bạn bè nhân dịp sinh nhật bé Ỉn. <br/><br/> Linh đã rất hài lòng với chiếc bánh, cảm giác hài lòng vì đó là loại bánh mình đã từng mong muốn thử làm từ lâu thôi và cuối cùng đã làm được (vì thực ra bánh này dễ làm lắm). Tiếp đó Linh vui hơn khi chiếc bánh được bạn bè khen ngợi và xử lý chiếc bánh ngon lành. Linh đã từng ăn bánh này 2 lần ở tiệm bánh bên ngoài, nhưng cả 2 lần ăn chỉ là tò mò xem bánh có hương vị thế nào, và khi lần này làm tại nhà rồi thì mới phân tích rõ được sự khác biệt.', 'images/black/11.jpg', '1', '1', '0', '150000', '18', '1', '2016-05-25 22:43:53', '2016-06-01 04:47:17');
INSERT INTO `article` VALUES ('2', '2', '99', 'Capuchino', 'Cappuccino là một thức uống sang trọng và cầu kỳ. Tách dùng để thưởng thức cà phê Cappuccino phải được làm bằng đá hoặc sứ, có thành dầy để giữ nóng lâu cho cà phê bên trong. Tách phải được hâm nóng trước khi phục vụ.\r\nCappuccino là một thức uống sang trọng và cầu kỳ. Tách dùng để thưởng thức cà phê Cappuccino phải được làm bằng đá hoặc sứ, có thành dầy để giữ nóng lâu cho cà phê bên trong. Tách phải được hâm nóng trước khi phục vụ.\r\n\r\n<br/>Một tách cà phê Cappuccino bao gồm ba phần chính: cà phê espresso, sữa nóng và sữa sủi bọt. Để hoàn thiện khẩu vị, người ta thường rãi lên trên tách cà phê Cappuccino một ít bột ca cao hay bột quế. Người pha chế thường dùng khuôn hay thìa khuấy điệu nghệ trong lúc rắc bột để tạo thành các hình nghệ thuật (hình trái tim, đám mây, bướm,…).', 'images/made/361.jpg', '1', '1', '80000', '60000', '68', '1', '2016-05-25 22:44:27', '2016-06-01 03:46:46');
INSERT INTO `article` VALUES ('3', '4', '0', 'Mojito Kiwi', 'Mojito kiwi này vị rất dịu nhẹ, màu xanh trái cây mát mẻ và đặc biệt là trẻ em cũng uống được vì không có chất cồn.', 'images/made/311.jpg', '1', '1', '40000', '32000', '53', '1', '2016-05-25 22:45:27', '2016-06-01 04:45:29');
INSERT INTO `article` VALUES ('4', '9', '99', 'Cheese Cake', 'Cheesecake là một món tráng miệng bao gồm lớp nhân làm từ phô mai mềm và tươi (không hẳn lúc nào cũng dùng creamcheese ) và đế bằng bánh quy cứng (ví dụ như Graham cracker crust, một loại đế bánh rất thông dụng, được làm từ bánh quy ngọt Graham của Mỹ), bánh gatô hoặc bông lan. Nó có thể được nướng hoặc không, tùy thuộc vào từng loại. Cheesecake thường được làm ngọt bằng đường, có mùi vị hoặc được trang trí bằng trái cây, các loại hạt, nước sốt hoa quả và chocolate. Những mùi vị phổ biến của cheesecake gồm chanh, dâu hoặc toffee (kẹo bơ cứng).', 'images/made/101.jpg', '1', '1', '0', '120000', '22', '1', '2016-05-25 22:46:02', '2016-06-01 03:46:39');
INSERT INTO `article` VALUES ('5', '9', '99', 'Tiramisu', 'Tiramisu là một loại bánh ngọt tráng miệng rất nổi tiếng của nước Ý. Cái tên \"Tirami su\" được hiểu theo nghĩa là \"Nhặt tôi lên\". Bánh là một sự kết hợp hòa quyện giữa hương thơm của cà phê, rượu nhẹ và vị béo của trứng cùng kem phô mai. Chỉ cần ăn một miếng là sẽ cảm nhận được tất cả các hương vị đó hòa quyện cùng một lúc, chính vì thế mà người ta còn gọi nó là \"Thiên đường trong miệng của bạn\" (Heaven in your mouth).', 'images/made/22.jpg', '1', '1', '120000', '100000', '216', '1', '2016-05-25 22:46:25', '2016-05-30 10:47:47');
INSERT INTO `article` VALUES ('6', '7', '99', 'Sandwich', 'Bánh mì kẹp, còn gọi là bánh kẹp, bánh xăng-đuých hay xăng-uých (phiên âm từ tiếng Anh: sandwich), là đồ ăn thường có ít nhất hai lát bánh mì và những lớp kẹp, nhất là thịt, đồ biển, hay phó mát cùng với rau hay xà lách. Có thể dùng bánh mì không, hay có thể quét bơ, dầu, mù tạc hay đồ gia vị khác.', 'images/sandwich/beef21.jpg', '1', '1', '90000', '70000', '98', '1', '2016-05-25 22:47:07', '2016-05-27 17:56:26');
INSERT INTO `article` VALUES ('7', '7', '99', 'Sandwich 2', 'Bánh mì kẹp, còn gọi là bánh kẹp, bánh xăng-đuých hay xăng-uých (phiên âm từ tiếng Anh: sandwich), là đồ ăn thường có ít nhất hai lát bánh mì và những lớp kẹp, nhất là thịt, đồ biển, hay phó mát cùng với rau hay xà lách. Có thể dùng bánh mì không, hay có thể quét bơ, dầu, mù tạc hay đồ gia vị khác.', 'images/sandwich/beef21.jpg', '1', '1', '90000', '70000', '98', '1', '2016-05-25 22:47:07', '2016-05-30 05:03:52');
INSERT INTO `article` VALUES ('8', '5', '99', 'Sinh tố Đào\r\nSinh tố Đào', 'Cocktail với hương vị ngậy, mát của dừa\r\nCocktail với hương vị đào\r\n', 'images/iceblended/dao.jpg', '0', '1', '55000', '55000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 05:03:52');
INSERT INTO `article` VALUES ('9', '5', '99', 'Sinh tố Kiwi', 'Cocktail với hương vị kiwi\r\n', 'images/iceblended/kiwi.jpg', '0', '1', '60000', '50000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 05:03:52');
INSERT INTO `article` VALUES ('10', '5', '99', 'Sinh tố Việt quất\r\n', 'Việt quất\r\nCocktial với hương vị Việt quất\r\n', 'images/iceblended/vietquat.jpg', '0', '1', '50000', '40000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 05:03:52');
INSERT INTO `article` VALUES ('11', '5', '99', 'Sinh tố Chanh leo', 'Cocktail với hương vị chua nhẹ của chanh leo\r\n', 'images/iceblended/passion.jpg', '0', '1', '50000', '40000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 05:03:52');
INSERT INTO `article` VALUES ('12', '5', '99', 'Sinh tố Dâu tây', 'Cocktail với hương vị dâu\r\n', 'images/iceblended/scl2.jpg', '0', '1', '50000', '40000', '101', '1', '2016-05-25 22:47:07', '2016-05-30 16:53:19');
INSERT INTO `article` VALUES ('13', '5', '99', 'Sinh tố Dưa hấu', 'Cocktail với hương vị dưa hấu', 'images/hoaqua/duahau.jpg', '0', '1', '50000', '40000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 16:53:19');
INSERT INTO `article` VALUES ('14', '5', '99', 'Sinh tố Hồng Xiêm', 'Cocktail với hương vị hồng xiêm', 'images/hoaqua/hongxiem.jpg', '0', '1', '50000', '40000', '102', '1', '2016-05-25 22:47:07', '2016-05-31 03:27:09');
INSERT INTO `article` VALUES ('15', '9', '99', 'Bánh ngọt Cuộn chanh leo\r\n', 'Bánh cuộn vị chanh leo nhân phô mai, kem tươi và mứt chanh leo\r\n', 'images/banhngot/cuonchanhleo.jpg', '0', '1', '60000', '50000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 16:53:19');
INSERT INTO `article` VALUES ('16', '9', '99', 'Bánh ngọt Cuộn trà xanh', 'Bánh cuộn vị trà xanh nhân kem dừa raffaelo và hạt macadamia\r\n', 'images/banhngot/cuontraxanh.jpg', '0', '1', '80000', '70000', '101', '1', '2016-05-25 22:47:07', '2016-05-30 17:05:12');
INSERT INTO `article` VALUES ('17', '9', '99', 'Bánh ngọt Cuộn cafe', 'Bánh cuộn vị café nhân kem mocha phủ hạnh nhân ngào đường\r\n', 'images/banhngot/cuoncafe.jpg', '0', '1', '77000', '66000', '100', '1', '2016-05-25 22:47:07', '2016-05-30 16:53:19');

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `level` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `has_children` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', 'Đồ uống', '1', '0', '1', '1', '2016-05-25 20:54:30', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('2', 'Cafe', '2', '1', '0', '1', '2016-05-25 20:54:50', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('3', 'Đồ đá xay', '2', '1', '0', '1', '2016-05-25 20:55:09', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('4', 'Mojito', '2', '1', '0', '1', '2016-05-25 20:56:03', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('5', 'Nước hoa quả', '2', '1', '0', '1', '2016-05-25 20:56:03', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('6', 'Đồ ăn nhanh', '1', '0', '1', '1', '2016-05-25 20:56:03', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('7', 'Sandwich', '2', '6', '0', '1', '2016-05-25 20:56:03', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('8', 'Pizza', '2', '6', '0', '1', '2016-05-25 20:56:03', '2016-05-25 20:56:03');
INSERT INTO `category` VALUES ('9', 'Bánh ngọt', '1', '0', '1', '1', '2016-05-25 20:56:03', '2016-05-30 09:40:42');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(255) CHARACTER SET latin1 NOT NULL,
  `status` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `content` text,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Trang chủ', 'home', '1', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('2', 'Giới thiệu', 'introduction', '6', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('3', 'Thực đơn', 'menu', '2', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('4', 'Hướng dẫn mua hàng', 'payment-tutorial', '3', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('5', 'Video', 'video', '4', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('6', 'Liên hệ', 'contact', '5', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');
INSERT INTO `menu` VALUES ('7', 'Hỗ trợ', 'support', '7', null, '1', '2016-05-25 20:59:00', '2016-05-25 20:59:00');

-- ----------------------------
-- Table structure for rating
-- ----------------------------
DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `article_id` (`article_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `rating_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rating_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rating
-- ----------------------------
INSERT INTO `rating` VALUES ('1', '2', '3', '4', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('2', '2', '1', '4', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('4', '1', '3', '4', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('5', '3', '3', '3', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('6', '4', '3', '5', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('8', '5', '3', '5', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('9', '6', '1', '4', '2016-05-27 21:33:50', '2016-05-27 21:34:11');
INSERT INTO `rating` VALUES ('10', '6', '3', '5', '2016-05-27 17:56:08', '2016-05-27 17:56:08');

-- ----------------------------
-- Table structure for supporter
-- ----------------------------
DROP TABLE IF EXISTS `supporter`;
CREATE TABLE `supporter` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `birthday` date NOT NULL,
  `phone` varchar(255) NOT NULL,
  `gmail` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of supporter
-- ----------------------------
INSERT INTO `supporter` VALUES ('1', 'Le Thi Nga', 'Thieu Toan, Thieu Hoa', '0000-00-00', '01666516283', 'lengatkt56@gmail.com', 'ngangoc56', '2016-05-31 00:13:04', '2016-05-31 00:13:07');

-- ----------------------------
-- Table structure for transaction
-- ----------------------------
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `article_id` (`article_id`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaction
-- ----------------------------
INSERT INTO `transaction` VALUES ('1', '5', '3', 'Gooner', 'tybd@gmail.com', '10', '0987654321', 'Thieu Toan, Thieu hoa', 'success', '2016-05-27 02:18:52', '2016-05-27 02:18:59');
INSERT INTO `transaction` VALUES ('2', '5', '3', 'Gooner', 'tybd@gmail.com', '1', '0987654321', 'Thieu Toan, Thieu hoa', 'waiting', '2016-05-27 02:18:52', '2016-05-28 17:38:50');
INSERT INTO `transaction` VALUES ('3', '6', '3', 'Gooner', 'tybd@gmail.com', '11', '0987654321', 'Thieu Toan, Thieu hoa', 'success', '2016-05-27 02:18:52', '2016-05-27 02:18:59');
INSERT INTO `transaction` VALUES ('4', '4', '3', 'Gooner', 'tybd@gmail.com', '5', '0987654321', 'Thieu Toan, Thieu hoa', 'success', '2016-05-27 02:18:52', '2016-05-27 02:18:59');
INSERT INTO `transaction` VALUES ('5', '4', '3', 'Gooner', 'tybd@gmail.com', '3', '0987654321', 'Thieu Toan, Thieu hoa', 'success', '2016-05-27 02:18:52', '2016-05-27 02:18:59');
INSERT INTO `transaction` VALUES ('6', '3', '3', 'Gooner', 'tybd@gmail.com', '5', '0987654321', 'Thieu Toan, Thieu hoa', 'success', '2016-05-27 02:18:52', '2016-05-29 16:52:35');
INSERT INTO `transaction` VALUES ('7', '6', '3', 'Gooner', 'tybd@gmail.com', '1', '0987654321', 'Thieu Hoa, Thanh Hoa', 'cancelled', '2016-05-27 13:43:36', '2016-05-29 16:52:29');
INSERT INTO `transaction` VALUES ('8', '1', '10', 'Guest', 'takeawayguest@mgail.com', '1', '000000000', 'Ha Noi', 'waiting', '2016-05-30 10:02:39', '2016-05-30 10:02:39');
INSERT INTO `transaction` VALUES ('9', '14', '13', 'Gooner', 'takeawattester@gmail.com', '3', '000000000', 'Ha Noi', 'success', '2016-05-30 17:09:25', '2016-05-30 17:09:25');
INSERT INTO `transaction` VALUES ('10', '16', '13', 'Gooner', 'takeawattester@gmail.com', '2', '000000000', 'Ha Noi', 'waiting', '2016-05-30 17:09:25', '2016-05-30 17:09:25');
INSERT INTO `transaction` VALUES ('11', '5', '10', 'Jack', 'jack@gmail.com', '2', '0987654321', 'London', 'waiting', '2016-06-03 14:35:58', '2016-06-03 14:35:58');
INSERT INTO `transaction` VALUES ('12', '7', '10', 'Jack', 'jack@gmail.com', '3', '09876544321', 'London', 'success', '2016-06-03 14:39:55', '2016-06-03 14:39:55');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(255) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `birthday` date DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'ngalt', '123456', 'Nga Le', 'lengatkt56@gmail.com', '01666516283', 'Thieu Hoa, Thanh Hoa', '1993-01-01', '1', '2016-05-25 21:00:32', '2016-05-26 15:55:35');
INSERT INTO `user` VALUES ('3', 'hunglc', '123456', 'TYBD', 'tybd@gmail.com', '0987654321', 'Thieu Hoa, Thieu Hoa, Thanh Hoa', '1992-03-19', '1', '2016-05-26 15:55:35', '2016-05-30 11:24:08');
INSERT INTO `user` VALUES ('10', 'guest_only', '123456', 'Guest Only', 'takeawayguest@gmail.com', '000000000', 'Don\'t remove this Accont', '1993-01-01', '0', '2016-05-29 17:23:53', '2016-05-30 14:19:08');
INSERT INTO `user` VALUES ('13', 'tester', '123456', 'Tester', 'takeawattester@gmail.com', '000000000', 'Don\'t remove this account', '1993-01-01', '1', '2016-05-30 14:56:31', '2016-06-01 04:33:12');

-- ----------------------------
-- Table structure for visited_log
-- ----------------------------
DROP TABLE IF EXISTS `visited_log`;
CREATE TABLE `visited_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) DEFAULT NULL,
  `expired_time` datetime NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of visited_log
-- ----------------------------
INSERT INTO `visited_log` VALUES ('3', '::1', '2016-06-02 01:29:49', '2016-06-02 00:29:49', '2016-06-02 00:29:49');
INSERT INTO `visited_log` VALUES ('4', '::1', '2016-05-31 14:54:02', '2016-05-31 13:54:02', '2016-05-31 13:54:02');
INSERT INTO `visited_log` VALUES ('5', '::1', '2016-06-03 15:28:35', '2016-06-03 14:28:35', '2016-06-03 14:28:35');
INSERT INTO `visited_log` VALUES ('9', '::1', '2016-06-03 18:28:14', '2016-06-03 17:28:14', '2016-06-03 17:28:14');
